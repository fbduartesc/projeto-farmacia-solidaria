-- MySQL dump 10.11
--
-- Host: localhost    Database: farmacia
-- ------------------------------------------------------
-- Server version	5.0.41-community-nt

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categorias`
--

DROP TABLE IF EXISTS `categorias`;
CREATE TABLE `categorias` (
  `cod_categoria` int(10) unsigned NOT NULL auto_increment,
  `categoria` varchar(50) NOT NULL,
  PRIMARY KEY  (`cod_categoria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categorias`
--

LOCK TABLES `categorias` WRITE;
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` VALUES (1,'Faixa preta'),(2,'Sem faixa');
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forma_farmaceutica`
--

DROP TABLE IF EXISTS `forma_farmaceutica`;
CREATE TABLE `forma_farmaceutica` (
  `cod_forma_farmaceutica` int(10) unsigned NOT NULL auto_increment,
  `forma_farmaceutica` varchar(50) NOT NULL,
  PRIMARY KEY  (`cod_forma_farmaceutica`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forma_farmaceutica`
--

LOCK TABLES `forma_farmaceutica` WRITE;
/*!40000 ALTER TABLE `forma_farmaceutica` DISABLE KEYS */;
INSERT INTO `forma_farmaceutica` VALUES (1,'Xarope'),(2,'Comprimido revestido'),(4,'Creme dermatológico');
/*!40000 ALTER TABLE `forma_farmaceutica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genericos`
--

DROP TABLE IF EXISTS `genericos`;
CREATE TABLE `genericos` (
  `cod_generico` int(10) unsigned NOT NULL auto_increment,
  `generico` varchar(50) NOT NULL,
  PRIMARY KEY  (`cod_generico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `genericos`
--

LOCK TABLES `genericos` WRITE;
/*!40000 ALTER TABLE `genericos` DISABLE KEYS */;
INSERT INTO `genericos` VALUES (2,'abaconol'),(3,'Fabio Duarte');
/*!40000 ALTER TABLE `genericos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itens_acesso`
--

DROP TABLE IF EXISTS `itens_acesso`;
CREATE TABLE `itens_acesso` (
  `cod_item` int(10) unsigned NOT NULL auto_increment,
  `controller_metodo` varchar(30) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  PRIMARY KEY  (`cod_item`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `itens_acesso`
--

LOCK TABLES `itens_acesso` WRITE;
/*!40000 ALTER TABLE `itens_acesso` DISABLE KEYS */;
INSERT INTO `itens_acesso` VALUES (1,'usuario/index','Mostrar todos usuários'),(2,'usuario/teste','Mostrar teste'),(3,'usuario/add','Adicionar novo usuário'),(4,'usuario/edit','Editar todos usuários'),(5,'usuario/remove','Remover todos usuários'),(6,'usuario/altera_senha','Alterar todas as senhas dos usuários'),(7,'paciente/add','Adicionar novo paciente'),(8,'paciente/index','Mostrar todos pacientes'),(9,'paciente/edit','Editar todos pacientes'),(10,'paciente/remove','Remover todos pacientes'),(11,'generico/add','Adicionar novo medicamento genérico'),(12,'produto/index','Mostrar produtos'),(13,'generico/index','Mostrar todos medicamentos genéricos'),(14,'generico/edit','Alterar todos os medicamentos genéricos'),(15,'generico/remove','Remover todos os medicamentos genéricos'),(16,'forma_farmaceutica/index','Mostrar todas formas farmacêuticas'),(17,'forma_farmaceutica/add','Adicionar nova forma farmacêutica'),(18,'forma_farmaceutica/edit','Alterar forma farmacêutica'),(19,'forma_farmaceutica/remove','Remover todas forma farmacêutica'),(20,'categoria/index','Mostrar todas categorias'),(21,'categoria/add','Adicionar nova categoria'),(22,'categoria/edit','Alterar todas categorias'),(23,'categoria/remove','Remover todas categorias');
/*!40000 ALTER TABLE `itens_acesso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pacientes`
--

DROP TABLE IF EXISTS `pacientes`;
CREATE TABLE `pacientes` (
  `cod_paciente` int(10) unsigned NOT NULL auto_increment,
  `usuarios_cod_usuario` int(10) unsigned NOT NULL,
  `nome` varchar(45) NOT NULL,
  `endereco` varchar(255) default NULL,
  `cep` varchar(45) default NULL,
  `bairro` varchar(45) default NULL,
  `cidade` varchar(45) default NULL,
  `email` varchar(100) default NULL,
  `data_nasc` date NOT NULL,
  `data_cadastro` date NOT NULL,
  `estado` varchar(45) NOT NULL,
  `ativo` int(10) unsigned NOT NULL default '1',
  `telefone_residencial` varchar(10) default NULL,
  `telefone_celular` varchar(10) default NULL,
  `telefone_comercial` varchar(10) default NULL,
  PRIMARY KEY  (`cod_paciente`),
  KEY `pacientes_FKIndex1` (`usuarios_cod_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pacientes`
--

LOCK TABLES `pacientes` WRITE;
/*!40000 ALTER TABLE `pacientes` DISABLE KEYS */;
INSERT INTO `pacientes` VALUES (1,1,'Fabio','eugenio da cruz','88817270','VIla Zuleima','Criciuma','fabio@unesc.net','1985-06-16','2009-02-13','',1,NULL,NULL,NULL),(2,1,'Driele Nazzari','Eugenio da Cruz','88817270','Vila Zuleima','Criciuma','fabio@unesc.net','1985-06-16','2009-02-16','SC - Santa Catarina',1,'34553535','',''),(3,1,'Teste','teste','28745454545','testet','tetste','teste@teste.com','2009-02-02','2009-02-16','SC - Santa Catarina',1,NULL,NULL,NULL),(4,1,'Usuario','naos ei','88817270','nao sei','Criciuma','teste@unesc.net','2009-10-10','2009-02-16','SC - Santa Catarina',1,NULL,NULL,NULL),(5,1,'Usuario','naos ei','88817270','nao sei','Criciuma','teste@unesc.net','2009-10-10','2009-02-16','SC - Santa Catarina',1,NULL,NULL,NULL),(6,1,'Maria Helena Duarte de Souza','Eugênio da Cruz, 34','88817270','Vila Zuleima','Criciúma','maria@maria.com','2009-10-10','2009-02-17','SC - Santa Catarina',1,'','4896049895','4834335030'),(7,16,'Paciente','paciente','88817270','Vila Zuleima','Criciuma','','2009-02-20','2009-02-17','SC - Santa Catarina',1,'','','');
/*!40000 ALTER TABLE `pacientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario_item_acesso`
--

DROP TABLE IF EXISTS `usuario_item_acesso`;
CREATE TABLE `usuario_item_acesso` (
  `itens_acesso_cod_item` int(10) unsigned NOT NULL,
  `usuarios_cod_usuario` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`itens_acesso_cod_item`,`usuarios_cod_usuario`),
  KEY `tipo_usuario_item_acesso_FKIndex2` (`itens_acesso_cod_item`),
  KEY `usuario_item_acesso_FKIndex2` (`usuarios_cod_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usuario_item_acesso`
--

LOCK TABLES `usuario_item_acesso` WRITE;
/*!40000 ALTER TABLE `usuario_item_acesso` DISABLE KEYS */;
INSERT INTO `usuario_item_acesso` VALUES (1,1),(1,2),(1,3),(1,14),(1,15),(1,16),(1,17),(1,18),(1,19),(1,20),(2,14),(2,16),(2,18),(3,1),(3,14),(3,16),(3,18),(4,1),(4,14),(4,16),(4,18),(4,20),(5,1),(5,14),(5,16),(5,18),(6,1),(6,2),(6,16),(6,18),(6,20),(7,1),(7,16),(8,1),(8,16),(8,20),(9,1),(9,16),(10,1),(10,16),(11,1),(12,1),(13,1),(13,2),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1);
/*!40000 ALTER TABLE `usuario_item_acesso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `cod_usuario` int(10) unsigned NOT NULL auto_increment,
  `nome` varchar(50) NOT NULL,
  `login` varchar(50) NOT NULL,
  `senha` varchar(50) NOT NULL,
  `telefone_contato` varchar(10) default NULL,
  `email` varchar(50) default NULL,
  `ativo` int(10) unsigned NOT NULL default '1',
  `data_cadastro` date NOT NULL,
  PRIMARY KEY  (`cod_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'Fabio Duarte','fabio','a53bd0415947807bcb95ceec535820ee','4896049895','fabio@unesc.net',1,'2009-02-05'),(2,'Estagiario','estagiario','6a1aa83cd6e67d59be423969975e9fab','4896049895','estagiario@unesc.net',1,'2009-02-05'),(3,'Usuário Restrito','restrito','a53bd0415947807bcb95ceec535820ee','34312550','restrito@unesc.net',1,'2009-02-06'),(14,'teste','teste','698dc19d489c4e4db73e28a713eab07b','','',1,'2009-02-06'),(15,'Eu - Usuário de teste','eu','4829322d03d1606fb09ae9af59a271d3','4834334407','eu@eu.com',1,'2009-02-09'),(16,'nois','nois','25027061df4fe22a684b6cb142a5f6d8','4896049895','nois@nois.com',1,'2009-02-09'),(17,'te','te','569ef72642be0fadd711d6a468d68ee1','4896','df@fa.com',0,'2009-02-09'),(18,'Laênio','laenio','bb5ee36214c1f7cce179edfc92c7af0e','91146578','laenio.alex@unesc.net',1,'2009-02-12'),(19,'Priscyla','pri','e060bb629c10e1b143614cc1e9ccdc67','4834312626','pri@unesc.net',1,'2009-02-12'),(20,'Indianara','indi','ac5e72357115f00c0bc6ac83bb203127','','',1,'2009-02-13');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2009-02-19 14:35:42
