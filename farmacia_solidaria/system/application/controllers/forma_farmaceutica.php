<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Forma_farmaceutica extends MY_Controller {

	function Forma_farmaceutica()
	{
		parent::MY_Controller();		
		$this->load->library('layout', 'layout/layout');
		$this->load->library("auth");
				
	}

	function index()
	{
		$acesso = $this->auth->checkAcesso();
		if($acesso)
		{
			$this->load->model('Forma_farmaceutica_model');						
			$data['titulo'] = "Forma farmac�utica cadastrado no sistema";
			$num_rows = $this->Forma_farmaceutica_model->num_linhas();
			$data['num_linhas'] = $num_rows;
			
			$this->load->library('pagination');
						
			$config['base_url'] = base_url().'/index.php/forma_farmaceutica/index/';
			$config['total_rows'] = $num_rows;
			$config['per_page'] = '10'; 
			$config['first_link'] = 'Primeiro';
			$config['last_link'] = '�ltimo';
			$config['num_links'] = 2;
			$config['next_link'] = '>';
			$config['prev_link'] = '<';		

			$this->pagination->initialize($config); 

			$data['paginacao'] = $this->pagination->create_links();
			$inicio = $this->uri->segment(3);
			if(Empty($inicio))
			{
				$inicio = 0;
			}		
			$data['query'] = $this->Forma_farmaceutica_model->paginacao($inicio, $config['per_page'] );
			
			$this->layout->view('forma_farmaceutica/index', $data);
		}else
		{
			$data['titulo'] = "Sem permiss�o de acesso.";
			$this->layout->view('usuario/restrito', $data);
		}
	}		
		
	function add()
	{		
		$acesso = $this->auth->checkAcessoOpcao('add');			
		if($acesso)
		{
		$this->load->model('Forma_farmaceutica_model');
		
		$data['titulo'] = "Adicionar Forma farmac�utica";	
				
		$forma_farmaceutica = $this->input->post('forma_farmaceutica');		

		$rules['forma_farmaceutica']	= "required";						

		$this->validation->set_rules($rules);		

		$fields['forma_farmaceutica'] = 'Forma farmac�utica';		
		
		$this->validation->set_fields($fields);				
		if ($this->validation->run() == FALSE)
		{			
			$this->layout->view('forma_farmaceutica/add', $data);
		}
		else
		{	
			$data['msg_cadastro'] = "Forma farmac�utica cadastrado com sucesso.";							
			$this->Forma_farmaceutica_model->adicionar($forma_farmaceutica);					
			$this->layout->view('forma_farmaceutica/add', $data);
			
		}
		}else
		{
			$data['titulo'] = "Sem permiss�o de acesso.";
			$this->layout->view('usuario/restrito', $data);
		}
	}
	
	function edit()
	{
		$id = $this->uri->segment(3);
		if(Empty($id))
		{
			$id = $this->input->post('forma_farmaceutica_hidden');
		}
		$acesso = $this->auth->checkAcessoOpcao('edit');			
		if($acesso)
		{
		$this->load->model('Forma_farmaceutica_model');
		
		$data['titulo'] = "Editar Forma Farmac�utica";
				
		// Busca informa��es sobre o usu�rio
		$query = $this->Forma_farmaceutica_model->busca_forma_farmaceutica_codigo($id);
		
		if(!Empty($query))
		{		
			//$query1 = $this->Paciente_model->busca_telefones_paciente($id);
			
			$data['codigo'] = $query->cod_forma_farmaceutica;		
					
			$id = $this->input->post('forma_farmaceutica_hidden');
			
			$forma_farmaceutica = $this->input->post('forma_farmaceutica');
			
			$rules['forma_farmaceutica']	= "required";				
			
			$this->validation->set_rules($rules);		

			$fields['forma_farmaceutica'] = 'Forma farmac�utica';			
			
			$this->validation->set_fields($fields);	
		
			$this->validation->forma_farmaceutica = $query->forma_farmaceutica;		
									
			if ($this->validation->run() == FALSE)
			{			
				$this->layout->view('forma_farmaceutica/edit', $data);
			}
			else
			{				
				$data['msg_cadastro'] = "Altera��es realizadas com sucesso.";							
				$this->Forma_farmaceutica_model->editar($id, $forma_farmaceutica);				
				$this->layout->view('forma_farmaceutica/edit', $data);
			}
			}else
			{
				$data['titulo'] = "Sem permiss�o de acesso.";
				$this->layout->view('usuario/restrito', $data);
			}
		}
		
	}
	
	function remove($id)
	{
		$acesso = $this->auth->checkAcessoOpcao('remove');			
		if($acesso)
		{
		$this->load->model('Forma_farmaceutica_model');
		//busca o id para ver se existe o usuario		
		$busca_forma_farmaceutica = $this->Forma_farmaceutica_model->busca_forma_farmaceutica_codigo($id);
		$data['titulo'] = "Remover forma farmac�utica";
		
		if(Empty($busca_forma_farmaceutica))
		{
			$data['msg'] = "Forma farmac�utica inexistente ou c�digo inv�lido.";
			$this->layout->view('forma_farmaceutica/remove', $data);
		
		}else
		{		
			$data['msg'] = "Forma farmac�utica removido com sucesso.";
			$this->Forma_farmaceutica_model->remover($id);
			$this->layout->view('forma_farmaceutica/remove', $data);
		}
		}else
		{
			$data['titulo'] = "Sem permiss�o de acesso.";
			$this->layout->view('usuario/restrito', $data);
		}		
	}
	
	function search()
	{
		$this->load->library('pagination');		
		$this->load->model('Forma_farmaceutica_model');				
		
		// grava o valor da busca em uma variavel global
		$this->search = $this->input->post('search');
				
		$data['titulo'] = ".: Farm�cia Solid�ria :. - Resultado da busca";
		
		$inicio = $this->uri->segment(3);
			
		$num_rows = $this->Forma_farmaceutica_model->num_linhas_busca($this->search);		
		$data['num_linhas'] = $num_rows;
		
		$config['base_url'] = base_url().'/index.php/forma_farmaceutica/search/';
		$config['per_page'] = '5'; 
        $config['total_rows'] = $num_rows;
		$config['first_link'] = 'Primeiro';
		$config['last_link'] = '�ltimo';
		$config['num_links'] = 2;
		$config['next_link'] = '>';
		$config['prev_link'] = '<';	
		//$config['uri_segment'] = 4;		
		
		if(Empty($inicio))
		{
			$inicio = 0;
		}
	
		$this->pagination->initialize($config); 

		$data['paginacao'] = $this->pagination->create_links();
			
		$data['busca'] = $this->Forma_farmaceutica_model->busca($this->search, $inicio, $config['per_page']);
		
		$this->layout->view('forma_farmaceutica/search', $data);
		
		
	}
}
?>