<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Generico extends MY_Controller {

	function Generico()
	{
		parent::MY_Controller();		
		$this->load->library('layout', 'layout/layout');
		$this->load->library("auth");
				
	}

	function index()
	{
		$acesso = $this->auth->checkAcesso();
		if($acesso)
		{
			$this->load->model('Generico_model');						
			$data['titulo'] = "Gen�ricos cadastrados no sistema";
			$num_rows = $this->Generico_model->num_linhas();
			$data['num_linhas'] = $num_rows;
			
			$this->load->library('pagination');
						
			$config['base_url'] = base_url().'/index.php/generico/index/';
			$config['total_rows'] = $num_rows;
			$config['per_page'] = '10'; 
			$config['first_link'] = 'Primeiro';
			$config['last_link'] = '�ltimo';
			$config['num_links'] = 2;
			$config['next_link'] = '>';
			$config['prev_link'] = '<';		

			$this->pagination->initialize($config); 

			$data['paginacao'] = $this->pagination->create_links();
			$inicio = $this->uri->segment(3);
			if(Empty($inicio))
			{
				$inicio = 0;
			}		
			$data['query'] = $this->Generico_model->paginacao($inicio, $config['per_page'] );
			
			$this->layout->view('generico/index', $data);
		}else
		{
			$data['titulo'] = "Sem permiss�o de acesso.";
			$this->layout->view('usuario/restrito', $data);
		}
	}		
		
	function add()
	{		
		$acesso = $this->auth->checkAcessoOpcao('add');			
		if($acesso)
		{
		$this->load->model('Generico_model');
		
		$data['titulo'] = "Adicionar Gen�rico";	
				
		$generico = $this->input->post('generico');		

		$rules['generico']	= "required";						

		$this->validation->set_rules($rules);		

		$fields['generico'] = 'Gen�rico';		
		
		$this->validation->set_fields($fields);				
		if ($this->validation->run() == FALSE)
		{			
			$this->layout->view('generico/add', $data);
		}
		else
		{	
			$data['msg_cadastro'] = "Medicamento gen�rico cadastrado com sucesso.";							
			$this->Generico_model->adicionar($generico);					
			$this->layout->view('generico/add', $data);
			
		}
		}else
		{
			$data['titulo'] = "Sem permiss�o de acesso.";
			$this->layout->view('usuario/restrito', $data);
		}
	}
	
	function edit()
	{
		$id = $this->uri->segment(3);
		if(Empty($id))
		{
			$id = $this->input->post('generico_hidden');
		}
		$acesso = $this->auth->checkAcessoOpcao('edit');			
		if($acesso)
		{
		$this->load->model('Generico_model');
		
		$data['titulo'] = "Editar Medicamento Gen�rico";
				
		// Busca informa��es sobre o usu�rio
		$query = $this->Generico_model->busca_generico_codigo($id);
		
		if(!Empty($query))
		{		
			//$query1 = $this->Paciente_model->busca_telefones_paciente($id);
			
			$data['codigo'] = $query->cod_generico;		
					
			$id = $this->input->post('generico_hidden');
			
			$generico = $this->input->post('generico');
			
			$rules['generico']	= "required";				
			
			$this->validation->set_rules($rules);		

			$fields['generico'] = 'Gen�rico';			
			
			$this->validation->set_fields($fields);	
		
			$this->validation->generico = $query->generico;		
									
			if ($this->validation->run() == FALSE)
			{			
				$this->layout->view('generico/edit', $data);
			}
			else
			{				
				$data['msg_cadastro'] = "Altera��es realizadas com sucesso.";							
				$this->Generico_model->editar($id, $generico);				
				$this->layout->view('generico/edit', $data);
			}
			}else
			{
				$data['titulo'] = "Sem permiss�o de acesso.";
				$this->layout->view('usuario/restrito', $data);
			}
		}
		
	}
	
	function remove($id)
	{
		$acesso = $this->auth->checkAcessoOpcao('remove');			
		if($acesso)
		{
		$this->load->model('Generico_model');
		//busca o id para ver se existe o usuario		
		$busca_generico = $this->Generico_model->busca_generico_codigo($id);
		$data['titulo'] = "Remover medicamento gen�rico";
		
		if(Empty($busca_generico))
		{
			$data['msg'] = "Medicamento gen�rico inexistente ou c�digo inv�lido.";
			$this->layout->view('generico/remove', $data);
		
		}else
		{		
			$data['msg'] = "Medicamento gen�rico removido com sucesso.";
			$this->Generico_model->remover($id);
			$this->layout->view('generico/remove', $data);
		}
		}else
		{
			$data['titulo'] = "Sem permiss�o de acesso.";
			$this->layout->view('usuario/restrito', $data);
		}		
	}
	
	function search()
	{
		$this->load->library('pagination');		
		$this->load->model('Generico_model');				
		
		// grava o valor da busca em uma variavel global
		$this->search = $this->input->post('search');
				
		$data['titulo'] = ".: Farm�cia Solid�ria :. - Resultado da busca";
		
		$inicio = $this->uri->segment(3);
			
		$num_rows = $this->Generico_model->num_linhas_busca($this->search);		
		$data['num_linhas'] = $num_rows;
		
		$config['base_url'] = base_url().'/index.php/generico/search/';
		$config['per_page'] = '5'; 
        $config['total_rows'] = $num_rows;
		$config['first_link'] = 'Primeiro';
		$config['last_link'] = '�ltimo';
		$config['num_links'] = 2;
		$config['next_link'] = '>';
		$config['prev_link'] = '<';	
		//$config['uri_segment'] = 4;		
		
		if(Empty($inicio))
		{
			$inicio = 0;
		}
	
		$this->pagination->initialize($config); 

		$data['paginacao'] = $this->pagination->create_links();
			
		$data['busca'] = $this->Generico_model->busca($this->search, $inicio, $config['per_page']);
		
		$this->layout->view('generico/search', $data);
		
		
	}
}
?>