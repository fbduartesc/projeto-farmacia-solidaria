<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Categoria extends MY_Controller {

	function Categoria()
	{
		parent::MY_Controller();		
		$this->load->library('layout', 'layout/layout');
		$this->load->library("auth");
				
	}

	function index()
	{
		$acesso = $this->auth->checkAcesso();
		if($acesso)
		{
			$this->load->model('Categoria_model');						
			$data['titulo'] = "Categorias cadastradas no sistema";
			$num_rows = $this->Categoria_model->num_linhas();
			$data['num_linhas'] = $num_rows;
			
			$this->load->library('pagination');
						
			$config['base_url'] = base_url().'/index.php/categoria/index/';
			$config['total_rows'] = $num_rows;
			$config['per_page'] = '10'; 
			$config['first_link'] = 'Primeiro';
			$config['last_link'] = '�ltimo';
			$config['num_links'] = 2;
			$config['next_link'] = '>';
			$config['prev_link'] = '<';		

			$this->pagination->initialize($config); 

			$data['paginacao'] = $this->pagination->create_links();
			$inicio = $this->uri->segment(3);
			if(Empty($inicio))
			{
				$inicio = 0;
			}		
			$data['query'] = $this->Categoria_model->paginacao($inicio, $config['per_page'] );
			
			$this->layout->view('categoria/index', $data);
		}else
		{
			$data['titulo'] = "Sem permiss�o de acesso.";
			$this->layout->view('usuario/restrito', $data);
		}
	}		
		
	function add()
	{		
		$acesso = $this->auth->checkAcessoOpcao('add');			
		if($acesso)
		{
		$this->load->model('Categoria_model');
		
		$data['titulo'] = "Adicionar Categoria";	
				
		$categoria = $this->input->post('categoria');		

		$rules['categoria']	= "required";						

		$this->validation->set_rules($rules);		

		$fields['categoria'] = 'Categoria';		
		
		$this->validation->set_fields($fields);				
		if ($this->validation->run() == FALSE)
		{			
			$this->layout->view('categoria/add', $data);
		}
		else
		{	
			$data['msg_cadastro'] = "Categoria cadastrada com sucesso.";							
			$this->Categoria_model->adicionar($categoria);					
			$this->layout->view('categoria/add', $data);
			
		}
		}else
		{
			$data['titulo'] = "Sem permiss�o de acesso.";
			$this->layout->view('usuario/restrito', $data);
		}
	}
	
	function edit()
	{
		$id = $this->uri->segment(3);
		if(Empty($id))
		{
			$id = $this->input->post('categoria_hidden');
		}
		$acesso = $this->auth->checkAcessoOpcao('edit');			
		if($acesso)
		{
		$this->load->model('Categoria_model');
		
		$data['titulo'] = "Editar Categoria";
				
		// Busca informa��es sobre o usu�rio
		$query = $this->Categoria_model->busca_categoria_codigo($id);
		
		if(!Empty($query))
		{		
			//$query1 = $this->Paciente_model->busca_telefones_paciente($id);
			
			$data['codigo'] = $query->cod_categoria;		
					
			$id = $this->input->post('categoria_hidden');
			
			$categoria = $this->input->post('categoria');
			
			$rules['categoria']	= "required";				
			
			$this->validation->set_rules($rules);		

			$fields['categoria'] = 'Categoria';			
			
			$this->validation->set_fields($fields);	
		
			$this->validation->categoria = $query->categoria;		
									
			if ($this->validation->run() == FALSE)
			{			
				$this->layout->view('categoria/edit', $data);
			}
			else
			{				
				$data['msg_cadastro'] = "Altera��es realizadas com sucesso.";							
				$this->Categoria_model->editar($id, $categoria);				
				$this->layout->view('categoria/edit', $data);
			}
			}else
			{
				$data['titulo'] = "Sem permiss�o de acesso.";
				$this->layout->view('usuario/restrito', $data);
			}
		}
		
	}
	
	function remove($id)
	{
		$acesso = $this->auth->checkAcessoOpcao('remove');			
		if($acesso)
		{
		$this->load->model('Categoria_model');
		//busca o id para ver se existe o usuario		
		$busca_categoria = $this->Generico_model->busca_categoria_codigo($id);
		$data['titulo'] = "Remover categoria";
		
		if(Empty($busca_categoria))
		{
			$data['msg'] = "Categoria inexistente ou c�digo inv�lido.";
			$this->layout->view('categoria/remove', $data);
		
		}else
		{		
			$data['msg'] = "Categoria removido com sucesso.";
			$this->Categoria_model->remover($id);
			$this->layout->view('categoria/remove', $data);
		}
		}else
		{
			$data['titulo'] = "Sem permiss�o de acesso.";
			$this->layout->view('usuario/restrito', $data);
		}		
	}
	
	function search()
	{
		$this->load->library('pagination');		
		$this->load->model('Categoria_model');				
		
		// grava o valor da busca em uma variavel global
		$this->search = $this->input->post('search');
				
		$data['titulo'] = ".: Farm�cia Solid�ria :. - Resultado da busca";
		
		$inicio = $this->uri->segment(3);
			
		$num_rows = $this->Categoria_model->num_linhas_busca($this->search);		
		$data['num_linhas'] = $num_rows;
		
		$config['base_url'] = base_url().'/index.php/categoria/search/';
		$config['per_page'] = '5'; 
        $config['total_rows'] = $num_rows;
		$config['first_link'] = 'Primeiro';
		$config['last_link'] = '�ltimo';
		$config['num_links'] = 2;
		$config['next_link'] = '>';
		$config['prev_link'] = '<';	
		//$config['uri_segment'] = 4;		
		
		if(Empty($inicio))
		{
			$inicio = 0;
		}
	
		$this->pagination->initialize($config); 

		$data['paginacao'] = $this->pagination->create_links();
			
		$data['busca'] = $this->Categoria_model->busca($this->search, $inicio, $config['per_page']);
		
		$this->layout->view('categoria/search', $data);
		
		
	}
}
?>