<?php
 
class Logar extends MY_Controller
{
	function __construct()
	{
		parent::MY_Controller();		
		//$this->load->helper('form');
		// Carrega o layout do sistema
		$this->load->library('layout', 'layout/layout');	
		
	}
	
	function login()
	{	
		// Destr�i todas as sess�es abertas
		$this->_destroySession();		
		
		$data['titulo'] = "Sistema Farm�cia Solid�ria";
		$data['nome'] = $this->session->userdata('nome_user');		
		$data['seg'] = $this->uri->segment(1);
		
		$usuario = $this->input->post('usuario');
		$secreto = $this->input->post('secreto');		
		$captchainconf = $this->input->post('captchainconf');
		
	
		$captcha = $this->_captcha();
		$data['captcha']       = $captcha['image'];
		$data['captchainconf'] = $captcha['word'];
		
		$senha = md5($this->input->post('senha'));
		
		$rules['usuario']	= "required";
		$rules['senha']	= "required";
		$rules['secreto']	= "required";
		
		$this->validation->set_rules($rules);		
		$fields['usuario'] = 'Login';
		$fields['senha'] = 'Senha';
		$fields['secreto'] = 'C�digo secreto';
		
		$this->validation->set_fields($fields);	
		
		if ($this->validation->run() == FALSE)
		{			
			$this->layout->view('login/login', $data);
		}else
		{				
			if(strcmp($captchainconf, $secreto) != 0)
			{
				$data['erro_usuario_senha'] = "O c�digo digitado est� incorreto.";
				$this->layout->view('login/login', $data);	
			}else
			{			
				$this->load->model('Logar_model');			
				$query = $this->Logar_model->verifica($usuario, $senha);
				if(!Empty($query))
				{	
					
					$codigo = $query->cod_usuario;
					$nome = $query->nome;					
					
					$dados = array(
									'codigo_user' => $codigo,
									'nome_user' => $nome,
									'esta_logado' => TRUE									
								);
					$this->session->set_userdata($dados);					
					redirect('index.php/usuario/index');
					
				}else
				{				
					$data['erro_usuario_senha'] = "Usu�rio ou senha inv�lido.";
					$this->layout->view('login/login', $data);				
				}
			}
		}
		
	}
	function _captcha() {
		$this->load->plugin('captcha');			
		$vals = array (
			'word' => '',
			'img_path' => './system/captcha/',
			'img_url' => base_url().'/system/captcha/',			
			'font_path' => './system/fonts/arialbd.ttf',
			'font_size' => 5,
			'img_width' => '150',
			'img_height' => 30,
			'expiration' => 7200
		);

		$cap = create_captcha($vals);
		
		return $cap; 

	}
	
	function logout() {
		 $this->_destroySession();
		 redirect('index.php/logar/login');
	}
	
	function _registrySessions($codigo_user, $nome_user) {
		$data = array('codigo_user' => (int) $codigo_user,
					  'nome_user' => (string) $nome_user,
					  'esta_logado' => TRUE,
					  'captcha' => null
					  );
		
		$this->session->set_userdata($data);
	}
	
	function _destroySession() { 
		$data = array('codigo_user' => null,
					  'nome_user' => null,
					  'esta_logado' => FALSE,
					  'captcha' => null
					  );
		$this->session->unset_userdata($data);
	}		
}

?>