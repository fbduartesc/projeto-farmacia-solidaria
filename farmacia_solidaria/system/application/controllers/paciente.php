<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Paciente extends MY_Controller {

	function Paciente()
	{
		parent::MY_Controller();		
		$this->load->library('layout', 'layout/layout');
		$this->load->library("auth");
				
	}

	function index()
	{
		$acesso = $this->auth->checkAcesso();
		if($acesso)
		{
			$this->load->model('Paciente_model');						
			$data['titulo'] = "Pacientes cadastrados no sistema";
			$num_rows = $this->Paciente_model->num_linhas();
			$data['num_linhas'] = $num_rows;
			
			$this->load->library('pagination');
						
			$config['base_url'] = base_url().'/index.php/paciente/index/';
			$config['total_rows'] = $num_rows;
			$config['per_page'] = '10'; 
			$config['first_link'] = 'Primeiro';
			$config['last_link'] = '�ltimo';
			$config['num_links'] = 2;
			$config['next_link'] = '>';
			$config['prev_link'] = '<';		

			$this->pagination->initialize($config); 

			$data['paginacao'] = $this->pagination->create_links();
			$inicio = $this->uri->segment(3);
			if(Empty($inicio))
			{
				$inicio = 0;
			}		
			$data['query'] = $this->Paciente_model->paginacao($inicio, $config['per_page'] );
			
			$this->layout->view('paciente/index', $data);
		}else
		{
			$data['titulo'] = "Sem permiss�o de acesso.";
			$this->layout->view('usuario/restrito', $data);
		}
	}		
		
	function add()
	{		
		$acesso = $this->auth->checkAcessoOpcao('add');			
		if($acesso)
		{
		$this->load->model('Paciente_model');
		
		$data['titulo'] = "Adicionar Paciente";	
				
		$nome = $this->input->post('nome');
		$endereco = $this->input->post('endereco');
		$bairro = $this->input->post('bairro');
		$cep = $this->input->post('cep');
		$cidade = $this->input->post('cidade');
		$email = $this->input->post('email');
		$estado = $this->input->post('estado');
		$data_nasc = $this->input->post('data_nasc');
		$tel_residencial = $this->input->post('tel_residencial');
		$tel_celular = $this->input->post('tel_celular');
		$tel_comercial = $this->input->post('tel_comercial');

		$rules['nome']	= "required";				
		$rules['endereco']	= "required";						
		$rules['bairro']	= "required";		
		$rules['cep']	= "numeric|max_length[8]";
		$rules['email']	= "valid_email";
		$rules['cidade']	= "required";		
		$rules['data_nasc'] = "required";
		$rules['tel_residencial'] = "numeric|max_length[10]";
		$rules['tel_celular'] = "numeric|max_length[10]";
		$rules['tel_comercial'] = "numeric|max_length[10]";

		$this->validation->set_rules($rules);		

		$fields['nome'] = 'Nome';
		$fields['endereco'] = 'Endere�o';		
		$fields['bairro'] = 'Bairro';			
		$fields['cep'] = 'Cep';	
		$fields['email'] = 'Email';	
		$fields['cidade'] = 'Cidade';			
		$fields['data_nasc'] = 'Data de nascimento';
		$fields['tel_residencial'] = 'Telefone Residencial';
		$fields['tel_celular'] = 'Telefone Celular';
		$fields['tel_comercial'] = 'Telefone Comercial';
		
		$this->validation->set_fields($fields);				
		if ($this->validation->run() == FALSE)
		{			
			$this->layout->view('paciente/add', $data);
		}
		else
		{	
			$data['msg_cadastro'] = "Paciente cadastrado com sucesso.";							
			$this->Paciente_model->adicionar($nome, $endereco, $cep, $bairro, $cidade, $email, $data_nasc, $estado, $tel_residencial, $tel_celular, $tel_comercial);					
			$this->layout->view('paciente/add', $data);
			
		}
		}else
		{
			$data['titulo'] = "Sem permiss�o de acesso.";
			$this->layout->view('usuario/restrito', $data);
		}
	}
	
	function edit()
	{
		$id = $this->uri->segment(3);
		if(Empty($id))
		{
			$id = $this->input->post('paciente_hidden');
		}
		$acesso = $this->auth->checkAcessoOpcao('edit');			
		if($acesso)
		{
		$this->load->model('Paciente_model');
		
		$data['titulo'] = "Editar Paciente";
				
		// Busca informa��es sobre o usu�rio
		$query = $this->Paciente_model->busca_paciente_codigo($id);
		
		if(!Empty($query))
		{		
			//$query1 = $this->Paciente_model->busca_telefones_paciente($id);
			
			$data['codigo'] = $query->cod_paciente;		
					
			$id = $this->input->post('paciente_hidden');
			
			$nome = $this->input->post('nome');
			$endereco = $this->input->post('endereco');
			$bairro = $this->input->post('bairro');
			$cep = $this->input->post('cep');
			$cidade = $this->input->post('cidade');
			$email = $this->input->post('email');
			$estado = $this->input->post('estado');
			$data_nasc = $this->input->post('data_nasc');
			$tel_residencial = $this->input->post('tel_residencial');
			$tel_celular = $this->input->post('tel_celular');
			$tel_comercial = $this->input->post('tel_comercial');

			$rules['nome']	= "required";				
			$rules['endereco']	= "required";						
			$rules['bairro']	= "required";		
			$rules['cep']	= "numeric|max_length[8]";
			$rules['email']	= "valid_email";
			$rules['cidade']	= "required";		
			$rules['data_nasc'] = "required";
			$rules['tel_residencial'] = "numeric|max_length[10]";
			$rules['tel_celular'] = "numeric|max_length[10]";
			$rules['tel_comercial'] = "numeric|max_length[10]";

			$this->validation->set_rules($rules);		

			$fields['nome'] = 'Nome';
			$fields['endereco'] = 'Endere�o';		
			$fields['bairro'] = 'Bairro';			
			$fields['cep'] = 'Cep';	
			$fields['email'] = 'Email';	
			$fields['cidade'] = 'Cidade';			
			$fields['data_nasc'] = 'Data de nascimento';
			$fields['tel_residencial'] = 'Telefone Residencial';
			$fields['tel_celular'] = 'Telefone Celular';
			$fields['tel_comercial'] = 'Telefone Comercial';
			
			$this->validation->set_fields($fields);	
		
			$this->validation->nome = $query->nome;		
			$this->validation->endereco = $query->endereco;
			$this->validation->bairro = $query->bairro;
			$this->validation->cep = $query->cep;
			$this->validation->email = $query->email;
			$this->validation->cidade = $query->cidade;
			$this->validation->data_nasc = $query->data_nasc;
			$this->validation->tel_residencial = $query->telefone_residencial;
			$this->validation->tel_celular = $query->telefone_celular;
			$this->validation->tel_comercial = $query->telefone_comercial;
						
			if ($this->validation->run() == FALSE)
			{			
				$this->layout->view('paciente/edit', $data);
			}
			else
			{				
				$data['msg_cadastro'] = "Altera��es realizadas com sucesso.";							
				$this->Paciente_model->editar($id, $nome, $endereco, $bairro, $cep, $cidade, $email, $estado, $data_nasc, $tel_residencial, $tel_celular, $tel_comercial);				
				$this->layout->view('paciente/edit', $data);
			}
			}else
			{
				$data['titulo'] = "Sem permiss�o de acesso.";
				$this->layout->view('usuario/restrito', $data);
			}
		}
		
	}
	
	function remove($id)
	{
		$acesso = $this->auth->checkAcessoOpcao('remove');			
		if($acesso)
		{
		$this->load->model('Paciente_model');
		//busca o id para ver se existe o usuario		
		$busca_paciente = $this->Paciente_model->busca_paciente_codigo($id);
		$data['titulo'] = "Remover paciente";
		
		if(Empty($busca_paciente))
		{
			$data['msg'] = "Paciente inexistente ou c�digo inv�lido.";
			$this->layout->view('paciente/remove', $data);
		
		}else
		{		
			$data['msg'] = "Paciente removido com sucesso.";
			$this->Paciente_model->remover($id);
			$this->layout->view('paciente/remove', $data);
		}
		}else
		{
			$data['titulo'] = "Sem permiss�o de acesso.";
			$this->layout->view('usuario/restrito', $data);
		}		
	}
	
	function search()
	{
		$this->load->library('pagination');		
		$this->load->model('Paciente_model');				
		
		// grava o valor da busca em uma variavel global
		$this->search = $this->input->post('search');
				
		$data['titulo'] = ".: Farm�cia Solid�ria :. - Resultado da busca";
		
		$inicio = $this->uri->segment(3);
			
		$num_rows = $this->Paciente_model->num_linhas_busca($this->search);		
		$data['num_linhas'] = $num_rows;
		
		$config['base_url'] = base_url().'/index.php/paciente/search/';
		$config['per_page'] = '5'; 
        $config['total_rows'] = $num_rows;
		$config['first_link'] = 'Primeiro';
		$config['last_link'] = '�ltimo';
		$config['num_links'] = 2;
		$config['next_link'] = '>';
		$config['prev_link'] = '<';	
		//$config['uri_segment'] = 4;		
		
		if(Empty($inicio))
		{
			$inicio = 0;
		}
	
		$this->pagination->initialize($config); 

		$data['paginacao'] = $this->pagination->create_links();
			
		$data['busca'] = $this->Paciente_model->busca($this->search, $inicio, $config['per_page']);
		
		$this->layout->view('paciente/search', $data);
		
		
	}
}
?>