<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Post extends MY_Controller {

	function Post()
	{
		parent::MY_Controller();		
		$this->load->library('layout', 'layout/layout');			
	}

	function add()
	{		
		$this->load->model('Categoria_model');
		$this->load->model('Post_model');
		
		$data['titulo'] = "Adicionar Post";	
		
		$data['selecao'] = $this->Categoria_model->todas();
		
		$titulo = $this->input->post('titulo');
		$descricao = $this->input->post('descricao');
		$formato = $this->input->post('formato');
		$tamanho = $this->input->post('tamanho');
		$categoria = $this->input->post('categoria');
		$foto = $this->input->post('foto');		

		$rules['titulo']	= "required";				
		$rules['descricao']	= "required";				
		$rules['formato']	= "required";						

		$this->validation->set_rules($rules);		

		$fields['titulo'] = 'T�tulo do Post';
		$fields['descricao'] = 'Descri��o do Post';
		$fields['formato'] = 'Formato do arquivo';	
		
		$this->validation->set_fields($fields);				
		if ($this->validation->run() == FALSE)
		{			
			$this->layout->view('post/add', $data);
		}
		else
		{				
			$data['msg_cadastro'] = "Post cadastrado com sucesso.";				
			
			$this->Post_model->adicionar($categoria, $titulo, $descricao, $formato, $tamanho);			
					
			$config['upload_path'] = './public/uploads/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size'] = '5000';
			//$config['max_width'] = '1024';
			//$config['max_height'] = '768';
			
			$this->load->library('upload', $config);			
			
			$this->upload->do_upload();
			
			$id_post = $this->db->insert_id();
			
			$foto = './public/uploads/'.$id_post.$this->upload->file_ext;
			
			rename('./public/uploads/'.$this->upload->file_name, $foto);
			
			// Redimensiona a imagem que foi inserida
			$config_re['image_library'] = 'GD2';
			$config_re['source_image']	= $foto;
			$config_re['create_thumb'] = FALSE;
			$config_re['maintain_ratio'] = TRUE;
			$config_re['width']	 = 242;
			$config_re['height']	= 320;
			$config_re['quality'] = '100%';

			$this->load->library('image_lib', $config_re); 

			$this->image_lib->resize();
			
			
			$this->Post_model->update($foto, $id_post);
						
			$this->layout->view('post/add', $data);
		}		
	}	
	
	function add_link()
	{		
		$this->load->model('Post_model');
		$data['selecao'] = $this->Post_model->todos();
		$data['titulo'] = "Adicionar Link Post";

		$id_post = $this->input->post('post');
		$link = $this->input->post('link');
		$texto = $this->input->post('texto');
				
		$rules['link']	= "required";				
		$rules['texto']	= "required";
		
		$this->validation->set_rules($rules);		

		$fields['link'] = 'Link do Post';
		$fields['Texto'] = 'Texto do Link Post';	
		
		$this->validation->set_fields($fields);				
		if ($this->validation->run() == FALSE)
		{			
			$this->layout->view('post/add_link', $data);
		}else
		{
			$data['msg_cadastro'] = "Post cadastrado com sucesso.";	
			$this->Post_model->adicionar_link($id_post, $link, $texto);						
			$this->layout->view('post/add_link', $data);
		
		}
	}
}
?>