<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Usuario extends MY_Controller {

	function Usuario()
	{
		parent::MY_Controller();		
		$this->load->library('layout', 'layout/layout');
		$this->load->library("auth");
				
	}

	function index()
	{
		$acesso = $this->auth->checkAcesso();
		if($acesso)
		{
			$this->load->model('Usuario_model');						
			$data['titulo'] = "Usu�rios cadastrados no sistema";
			$num_rows = $this->Usuario_model->num_linhas();
			$data['num_linhas'] = $num_rows;
			
			$this->load->library('pagination');
						
			$config['base_url'] = base_url().'/index.php/usuario/index/';
			$config['total_rows'] = $num_rows;
			$config['per_page'] = '10'; 
			$config['first_link'] = 'Primeiro';
			$config['last_link'] = '�ltimo';
			$config['num_links'] = 2;
			$config['next_link'] = '>';
			$config['prev_link'] = '<';		

			$this->pagination->initialize($config); 

			$data['paginacao'] = $this->pagination->create_links();
			$inicio = $this->uri->segment(3);
			if(Empty($inicio))
			{
				$inicio = 0;
			}		
			$data['query'] = $this->Usuario_model->paginacao($inicio, $config['per_page'] );
			
			$this->layout->view('usuario/index', $data);
		}else
		{
			$data['titulo'] = "Sem permiss�o de acesso.";
			$this->layout->view('usuario/restrito', $data);
		}
	}		
		
	function add()
	{		
		$acesso = $this->auth->checkAcessoOpcao('add');			
		if($acesso)
		{
		$this->load->model('Usuario_model');
		
		$data['titulo'] = "Adicionar Usu�rio";	
		
		$data['permissoes'] = $this->Usuario_model->todas_permissoes();
		
		$permissoes =  $this->input->post('permissoes');		
		$nome = $this->input->post('nome');
		$login = $this->input->post('login');
		$senha = md5($this->input->post('senha'));
		$senhaconf = md5($this->input->post('senhaconf'));
		$telefone = $this->input->post('telefone');
		$email = $this->input->post('email');		

		$rules['nome']	= "required";				
		$rules['login']	= "required";				
		$rules['senha']	= "required";		
		$rules['senhaconf']	= "required|matches[senha]";
		$rules['email']	= "valid_email";
		$rules['telefone']	= "max_length[10]";

		$this->validation->set_rules($rules);		

		$fields['nome'] = 'Nome';
		$fields['login'] = 'Login';
		$fields['senha'] = 'Senha';	
		$fields['senhaconf'] = 'Confirma��o da senha';	
		$fields['email'] = 'Email';	
		$fields['telefone'] = 'Telefone';	
		
		$this->validation->set_fields($fields);				
		if ($this->validation->run() == FALSE)
		{			
			$this->layout->view('usuario/add', $data);
		}
		else
		{				
			$query = $this->Usuario_model->verifica_usuario($nome);
			if(Empty($query))
			{
				$data['msg_cadastro'] = "Usu�rio cadastrado com sucesso.";							
				$this->Usuario_model->adicionar($nome, $login, $senha, $telefone, $email);
				$user = $this->db->insert_id();			
				foreach($permissoes as $p):
					$this->Usuario_model->adicionar_permissao($p, $user);							
				endforeach;
							
				$this->layout->view('usuario/add', $data);
			}else
			{
				$data['msg_cadastro'] = "Usu�rio j� em uso.";	
				$this->layout->view('usuario/add', $data);
			}
		}
		}else
		{
			$data['titulo'] = "Sem permiss�o de acesso.";
			$this->layout->view('usuario/restrito', $data);
		}
	}
	
	function edit()
	{
		$id = $this->uri->segment(3);
		if(Empty($id))
		{
			$id = $this->input->post('usuario_hidden');
		}
		$acesso = $this->auth->checkAcessoOpcao('edit');			
		if($acesso || $id == $this->session->userdata('codigo_user'))
		{
		$this->load->model('Usuario_model');
		
		$data['titulo'] = "Editar Usu�rio";
				
		// Busca informa��es sobre o usu�rio
		$query = $this->Usuario_model->busca_usuario_codigo($id);
		
		$data['codigo'] = $query->cod_usuario;
		$data['todas_permissoes'] = $this->Usuario_model->todas_permissoes();
		$data['permi'] = $this->Usuario_model->permissoes_user($id);
		
		$permissoes =  $this->input->post('permissoes');	
		$id = $this->input->post('usuario_hidden');
		$nome = $this->input->post('nome');				
		$telefone = $this->input->post('telefone');
		$email = $this->input->post('email');		
				
		$rules['nome']	= "required";			
		
		$rules['email']	= "valid_email";
		$rules['telefone']	= "max_length[10]";

		$this->validation->set_rules($rules);		

		$fields['nome'] = 'Nome';	
		$fields['email'] = 'Email';	
		$fields['telefone'] = 'Telefone';	
		
		$this->validation->set_fields($fields);	
	
		$this->validation->nome = $query->nome;		
		$this->validation->telefone = $query->telefone_contato;
		$this->validation->email = $query->email;
			
		if ($this->validation->run() == FALSE)
		{			
			$this->layout->view('usuario/edit', $data);
		}
		else
		{				
			$data['msg_cadastro'] = "Altera��es realizadas com sucesso.";							
			$this->Usuario_model->editar($id, $nome, $telefone, $email);
			
			$acesso = $this->auth->checkAcessoOpcao('edit');			
			if($acesso)
			{
				$this->Usuario_model->editar_permissoes($permissoes, $id);
			}
			$data['permi'] = "";
			$this->layout->view('usuario/edit', $data);
		}
		}else
		{
			$data['titulo'] = "Sem permiss�o de acesso.";
			$this->layout->view('usuario/restrito', $data);
		}
		
	}
	
	function remove($id)
	{
		$acesso = $this->auth->checkAcessoOpcao('remove');			
		if($acesso)
		{
		$this->load->model('Usuario_model');
		//busca o id para ver se existe o usuario		
		$busca_usuario = $this->Usuario_model->busca_usuario_codigo($id);
		$data['titulo'] = "Remover usu�rio";
		
		if(Empty($busca_usuario))
		{
			$data['msg'] = "Usu�rio inexistente ou c�digo inv�lido.";
			$this->layout->view('usuario/remove', $data);
		
		}else
		{		
			$data['msg'] = "Usu�rio removido com sucesso.";
			$this->Usuario_model->remover($id);
			$this->layout->view('usuario/remove', $data);
		}
		}else
		{
			$data['titulo'] = "Sem permiss�o de acesso.";
			$this->layout->view('usuario/restrito', $data);
		}		
	}
	
	function altera_senha()
	{
		$id = $this->uri->segment(3);
		if(Empty($id))
		{
			$id = $this->input->post('usuario_hidden');
		}
		$acesso = $this->auth->checkAcessoOpcao('altera_senha');			
		if($acesso || $id == $this->session->userdata('codigo_user'))
		{
			$this->load->model('Usuario_model');
			$data['titulo']	 = "Alterar Senha";
			
			// Busca informa��es sobre o usu�rio
			$query = $this->Usuario_model->busca_usuario_codigo($id);
			$data['codigo'] = $query->cod_usuario;
			
			$id = $this->input->post('usuario_hidden');
			$senhaantiga = md5($this->input->post('senhaantiga'));				
			$novasenha = md5($this->input->post('novasenha'));
			$novasenhaconf = md5($this->input->post('novasenhaconf'));		
			
			if($query->cod_usuario == $this->session->userdata('codigo_user'))
			{
				$rules['senhaantiga']	= "required";
			}
			$rules['novasenha']	= "required";
			$rules['novasenhaconf']	= "required|matches[novasenha]";

			$this->validation->set_rules($rules);		

			if($query->cod_usuario == $this->session->userdata('codigo_user'))
			{
				$fields['senhaantiga'] = 'Senha Antiga';	
			}
			$fields['novasenha'] = 'Nova Senha';	
			$fields['novasenhaconf'] = 'Confirma Nova Senha';	
			
			$this->validation->set_fields($fields);
			
			if ($this->validation->run() == FALSE)
			{			
				$this->layout->view('usuario/form_senha', $data);
			}
			else
			{	
				if($query->cod_usuario == $this->session->userdata('codigo_user'))
				{
					if($senhaantiga == $query->senha)
					{
						$this->Usuario_model->editar_senha($id, $novasenha);
						$data['msg_cadastro'] = "Senha alterada com sucesso.";
						$this->layout->view('usuario/form_senha', $data);
					}else
					{					
						$data['msg_cadastro'] = "Senha antiga n�o confere.";
						$this->layout->view('usuario/form_senha', $data);
					}
				}else
				{					
					$this->Usuario_model->editar_senha($id, $novasenha);
					$data['msg_cadastro'] = "Senha alterada com sucesso.";
					$this->layout->view('usuario/form_senha', $data);
				}
			}
		}else
		{
			$data['titulo'] = "Sem permiss�o de acesso.";
			$this->layout->view('usuario/restrito', $data);
		}
	}
	
}
?>