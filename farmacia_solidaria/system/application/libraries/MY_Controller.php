<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends Controller 
{
	function MY_Controller() {
		parent::Controller();		

		$this->load->library(array("auth"));
		$this->auth->filterAuth();
	}
}

?>