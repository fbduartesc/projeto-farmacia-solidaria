<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Auth
{
	private $_CI = null;
	
	function __construct() {
		$this->_CI =& get_instance();	
	}
	
	/**
	 * Verifica se o usu�rio tem permiss�o de acesso
	 */
	function checkAuth() {		
		if($this->_CI->session->userdata("codigo_user") == "" || $this->_CI->session->userdata("nome_user") == "" || $this->_CI->session->userdata("esta_logado") == FALSE) 		
		{
			return False;			
		}else
		{
			return True;
		}
	}
	
	
	function checkAcesso()
	{
		
		$this->_CI->load->model('Usuario_model');			
		
		$valor = $this->_CI->uri->segment(1)."/".$this->_CI->uri->segment(2);
		$query = $this->_CI->Usuario_model->checkAcesso($valor);
		
		if(Empty($query))
		{
			return False;
		}else
		{
			return True;
		}
	}
	
	function checkAcessoOpcao($opcao)
	{
		$this->_CI->load->model('Usuario_model');			
		
		$valor = $this->_CI->uri->segment(1)."/".$opcao;
		$query = $this->_CI->Usuario_model->checkAcesso($valor);
		
		if(Empty($query))
		{
			return False;
		}else
		{
			return True;
		}
	}
	
	function filterAuth() 
	{
		if($this->_CI->uri->segment(1) != "logar")
		{			
			if(!$this->checkAuth())
			{				
				redirect("index.php/logar/login");
			}
			/*
			if(!$this->checkAcesso())
			{				
				$this->_CI->load->library('layout', 'layout/layout');
				$this->_CI->layout->view('usuario/restrito');
				
			}*/
			
		}
		
	}

	
}

?>