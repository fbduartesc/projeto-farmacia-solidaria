
<h2>Medicamentos gen�ricos 
<?php 
	$acesso = $this->auth->checkAcessoOpcao('add');
	if($acesso)
	{
?>
		- <a href="<?php echo base_url(); ?>index.php/generico/add">Adicionar</a>
<?php
	}
?>
</h2>


<?php if($num_linhas != 0){ 
?>

<table>
	<tr>
		<th>C�digo</th>		
		<th>Gen�rico</th>				
		<th>A��es</th>
	</tr>
	<?php 
		$cont=0;
		foreach($query as $row):
	?>
	<tr class="<?php if($cont % 2 == 0) echo 'rowA'; else echo 'rowB'; ?>">
		<td><?php echo $row->cod_generico; ?></td>		
		<td><?php echo $row->generico; ?></td>				
		<td>
		<?php 			
			$acesso = $this->auth->checkAcessoOpcao('edit');			
			if($acesso)
			{
		?>
			<a href="<?php echo base_url(); ?>index.php/generico/edit/<?php echo $row->cod_generico; ?>"><img src="<?php echo base_url();?>public/images/edit.png" alt="Editar Medicamento gen�rico" title="Editar Medicamento gen�rico" /></a>
		<?php
			}
			$acesso1 = $this->auth->checkAcessoOpcao('remove');
			if($acesso1)
			{
		?>	
			<a onclick="return (confirm('Voc� confirma a Exclus�o?'))" href="<?php echo base_url(); ?>index.php/generico/remove/<?php echo $row->cod_generico; ?>"><img src="<?php echo base_url();?>public/images/delete.png" alt="Apagar medicamento gen�rico" title="Apagar medicamento gen�rico" /></a></td>
		<?php }
		?>
	
	</tr>
	<?php
			$cont++;			
		endforeach;
		?>	
</table>
<?php
echo "Total de medicamentos gen�ricos encontrados: <strong>". $num_linhas. "</strong>";
echo "<div id='link' align='center'>".$paginacao."</div>";
}else
{
	echo "Nenhum medicamento gen�rico cadastrado.";
}
?>

</div>
	
	<?php $this->load->view('busca_generico'); ?>

