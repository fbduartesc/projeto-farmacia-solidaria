<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
Design by Free CSS Templates
http://www.freecsstemplates.org
Released for free under a Creative Commons Attribution 2.5 License
-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title><?php echo $titulo; ?></title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="<?php echo base_url();?>public/css/default.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url(); ?>public/js/jquery-1.3.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/js/funcoes.js" type="text/javascript"></script>

</head>
<body>
<div id="header">
	<div id="logo">
		<!-- <h1><span>Total</span>Download</h1>-->
		<h1><span>Farm�cia</span>Solid�ria</h1>
		<h2><a href="" title="P�gina Inicial">Beta</a></h2>
	</div>
	<div id="menu">
		<ul>
			
			<?php
			if(!$this->session->userdata('esta_logado')) echo '<li class="first"><a href="'.base_url().'index.php/logar/login" accesskey="2" title="Acessar sistema">Login</a></li>';
			?>			
			<?php if($this->session->userdata('esta_logado')) echo '
			<li><a href="'.base_url(). 'index.php/produto/index" title="">Produtos</a></li>
			<li><a href="'.base_url(). 'index.php/paciente/index" title="">Pacientes</a></li>			
			<li><a href="'.base_url(). 'index.php/usuario/index" title="">Usu�rios</a></li>			
			<li><a href="'.base_url(). 'index.php/logar/logout" accesskey="5" title="">Logout</a></li>'; ?>
			
		</ul>
	</div>
</div>
<div id="splash"><img src="<?php echo base_url();?>public/images/logo.jpg" alt="Total Download" title="Total Download" width="877" height="140" />
<?php if($this->session->userdata('esta_logado')) {?>
<strong>Logado como: </strong><?php echo $this->session->userdata('nome_user'); }?></div>

<div id="content">

	<div id="colOne">	
		<?php echo $content_for_layout; ?>			
	</div>
	
	<div style="clear: both;">&nbsp;</div>
</div>
<div id="footer">
	<p><?php echo date('Y');?> <a href="" title="Farm�cia Solid�ria">Farm�cia Solid�ria</a></p>
</div>
</body>
</html>
