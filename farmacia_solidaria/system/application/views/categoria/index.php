
<h2>Categorias
<?php 
	$acesso = $this->auth->checkAcessoOpcao('add');
	if($acesso)
	{
?>
		- <a href="<?php echo base_url(); ?>index.php/categoria/add">Adicionar</a>
<?php
	}
?>
</h2>


<?php if($num_linhas != 0){ 
?>

<table>
	<tr>
		<th>C�digo</th>		
		<th>Categoria</th>				
		<th>A��es</th>
	</tr>
	<?php 
		$cont=0;
		foreach($query as $row):
	?>
	<tr class="<?php if($cont % 2 == 0) echo 'rowA'; else echo 'rowB'; ?>">
		<td><?php echo $row->cod_categoria; ?></td>		
		<td><?php echo $row->categoria; ?></td>				
		<td>
		<?php 			
			$acesso = $this->auth->checkAcessoOpcao('edit');			
			if($acesso)
			{
		?>
			<a href="<?php echo base_url(); ?>index.php/categoria/edit/<?php echo $row->cod_categoria; ?>"><img src="<?php echo base_url();?>public/images/edit.png" alt="Editar Categoria" title="Editar Categoria" /></a>
		<?php
			}
			$acesso1 = $this->auth->checkAcessoOpcao('remove');
			if($acesso1)
			{
		?>	
			<a onclick="return (confirm('Voc� confirma a Exclus�o?'))" href="<?php echo base_url(); ?>index.php/categoria/remove/<?php echo $row->cod_categoria; ?>"><img src="<?php echo base_url();?>public/images/delete.png" alt="Apagar categoria" title="Apagar categoria" /></a></td>
		<?php }
		?>
	
	</tr>
	<?php
			$cont++;			
		endforeach;
		?>	
</table>
<?php
echo "Total de categorias encontrados: <strong>". $num_linhas. "</strong>";
echo "<div id='link' align='center'>".$paginacao."</div>";
}else
{
	echo "Nenhuma categoria cadastrado.";
}
?>

</div>
	
	<?php $this->load->view('busca_categoria'); ?>

