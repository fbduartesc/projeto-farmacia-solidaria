
<h2>Formas farmac�uticas
<?php 
	$acesso = $this->auth->checkAcessoOpcao('add');
	if($acesso)
	{
?>
		- <a href="<?php echo base_url(); ?>index.php/forma_farmaceutica/add">Adicionar</a>
<?php
	}
?>
</h2>


<?php if($num_linhas != 0){ 
?>

<table>
	<tr>
		<th>C�digo</th>		
		<th>Forma farmac�utica</th>				
		<th>A��es</th>
	</tr>
	<?php 
		$cont=0;
		foreach($query as $row):
	?>
	<tr class="<?php if($cont % 2 == 0) echo 'rowA'; else echo 'rowB'; ?>">
		<td><?php echo $row->cod_forma_farmaceutica; ?></td>		
		<td><?php echo $row->forma_farmaceutica; ?></td>				
		<td>
		<?php 			
			$acesso = $this->auth->checkAcessoOpcao('edit');			
			if($acesso)
			{
		?>
			<a href="<?php echo base_url(); ?>index.php/forma_farmaceutica/edit/<?php echo $row->cod_forma_farmaceutica; ?>"><img src="<?php echo base_url();?>public/images/edit.png" alt="Editar Forma Farmac�utica" title="Editar Forma Farmac�utica" /></a>
		<?php
			}
			$acesso1 = $this->auth->checkAcessoOpcao('remove');
			if($acesso1)
			{
		?>	
			<a onclick="return (confirm('Voc� confirma a Exclus�o?'))" href="<?php echo base_url(); ?>index.php/forma_farmaceutica/remove/<?php echo $row->cod_forma_farmaceutica; ?>"><img src="<?php echo base_url();?>public/images/delete.png" alt="Apagar forma farmac�utica" title="Apagar forma farmac�utica" /></a></td>
		<?php }
		?>
	
	</tr>
	<?php
			$cont++;			
		endforeach;
		?>	
</table>
<?php
echo "Total de forma farmac�utica encontrados: <strong>". $num_linhas. "</strong>";
echo "<div id='link' align='center'>".$paginacao."</div>";
}else
{
	echo "Nenhuma forma farmac�utica cadastrada.";
}
?>

</div>
	
	<?php $this->load->view('busca_forma_farmaceutica'); ?>

