<div id="formulario">

<?php if(!Empty($msg_cadastro)){ echo $msg_cadastro; }?>

<p>
<a href="<?php echo base_url(); ?>index.php/paciente/index">Voltar</a>
</p>

<form id="form1" method="post" action="/farmacia_solidaria/index.php/paciente/edit" >
<fieldset>
	<legend>Paciente - Editar</legend>
		
	    <label for="nome">Nome</label>
		<input name="nome" type="text" id="textfield1" value="<?php if(Empty($msg_cadastro)) { echo $this->validation->nome; }?>" /><br />
		<?php echo "<div class='erro'><strong>".$this->validation->nome_error."</strong></div>"; ?>
					
		<label for="endereco">Endere�o</label>
		<input name="endereco" type="text" id="textfield1" value="<?php if(Empty($msg_cadastro)) { echo $this->validation->endereco; }?>" /><br />
		<?php echo "<div class='erro'><strong>".$this->validation->endereco_error."</strong></div>"; ?>
				
		<label for="bairro">Bairro</label>
		<input name="bairro" type="text" id="textfield1" value="<?php if(Empty($msg_cadastro)) { echo $this->validation->bairro; }?>" /><br />
		<?php echo "<div class='erro'><strong>".$this->validation->bairro_error."</strong></div>"; ?>
		
		<label for="cidade">Cidade</label>
		<input name="cidade" type="text" id="textfield1" value="<?php if(Empty($msg_cadastro)) { echo $this->validation->cidade; }?>"/>
		<?php echo "<div class='erro'><strong>".$this->validation->cidade_error."</strong></div>"; ?>
		
		<label for="cep">Cep</label>
		<input name="cep" type="text" id="textfield1" value="<?php if(Empty($msg_cadastro)) { echo $this->validation->cep; }?>" />
		<?php echo "<div class='erro'><strong>".$this->validation->cep_error."</strong></div>"; ?>
				
		<label for="estado">Estado</label>
		<select name="estado"  id="textfield1" >			
			<option value="AC - Acre">Acre</option>
			<option value="AL - Alagoas">Alagoas</option>
			<option value="AP - Amap�">Amap�</option>
			<option value="AM - Amazonas">Amazonas</option>
			<option value="BA - Bahia">Bahia</option>
			<option value="CE - Cear�">Cear�</option>
			<option value="DF - Distrito Federal">Distrito Federal</option>
			<option value="ES - Espirito Santo">Espirito Santo</option>
			<option value="GO - Goi�s">Goi�s</option>
			<option value="MA - Maranh�o">Maranh�o</option>
			<option value="MS - Mato Grosso do Sul">Mato Grosso do Sul</option>
			<option value="MT - Mato Grosso">Mato Grosso</option>
			<option value="MG - Minas Gerais">Minas Gerais</option>
			<option value="PA - Par�">Par�</option>
			<option value="PB - Para�ba">Para�ba</option>
			<option value="PR - Paran�">Paran�</option>
			<option value="PE - Pernambuco">Pernambuco</option>
			<option value="PI - Piau�">Piau�</option>
			<option value="RJ - Rio de Janeiro">Rio de Janeiro</option>
			<option value="RN - Rio Grande do Norte">Rio Grande do Norte</option>
			<option value="RS - Rio Grande do Sul">Rio Grande do Sul</option>
			<option value="RO - Rond�nia">Rond�nia</option>
			<option value="RR - Roraima">Roraima</option>
			<option value="SC - Santa Catarina" selected="selected">Santa Catarina</option>
			<option value="SP - S�o Paulo">S�o Paulo</option>
			<option value="SE - Sergipe">Sergipe</option>
			<option value="TO - Tocantins">Tocantins</option>
		</select>
		
		
		<label for="data_nasc">Data de Nasc</label>
		<input name="data_nasc" type="text" id="textfield1" value="<?php if(Empty($msg_cadastro)) { echo $this->validation->data_nasc; }?>" /><br />		
		<?php echo "<div class='erro'><strong>".$this->validation->data_nasc_error."</strong></div>"; ?>
		
		<label for="email">Email</label>
		<input name="email" type="text" id="textfield1" value="<?php if(Empty($msg_cadastro)) { echo $this->validation->email; }?>" /><br />		
		<?php echo "<div class='erro'><strong>".$this->validation->email_error."</strong></div>"; ?>
		
		<label for="tel_residencial">Telefone Residencial</label>
		<input name="tel_residencial" type="text" id="textfield1" value="<?php if(Empty($msg_cadastro)) { echo $this->validation->tel_residencial; }?>" />
		<?php echo "<div class='erro'><strong>".$this->validation->tel_residencial_error."</strong></div>"; ?>
		<br />
		
		<label for="tel_celular">Telefone Celular</label>
		<input name="tel_celular" type="text" id="textfield1" value="<?php if(Empty($msg_cadastro)) { echo $this->validation->tel_celular; }?>" />
		<?php echo "<div class='erro'><strong>".$this->validation->tel_celular_error."</strong></div>"; ?>
		<br />
		
		<label for="tel_comercial">Telefone Comercial</label>
		<input name="tel_comercial" type="text" id="textfield1" value="<?php if(Empty($msg_cadastro)) { echo $this->validation->tel_comercial; }?>" />
		<?php echo "<div class='erro'><strong>".$this->validation->tel_comercial_error."</strong></div>"; ?>
		<br />
		
		<input name="edit" type="submit" id="submit1" value="Editar" />
		<input name="limpar" type="reset" id="submit1" value="Limpar" />
		<input type="hidden" id="paciente_hidden" name="paciente_hidden" value="<?php echo $codigo; ?>" />

</fieldset>	
</form>
</div>
<p></p>