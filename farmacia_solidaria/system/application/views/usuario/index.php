<h2>Usu�rios 
<?php 
	$acesso = $this->auth->checkAcessoOpcao('add');
	if($acesso)
	{
?>
		- <a href="<?php echo base_url(); ?>index.php/usuario/add">Adicionar</a>
<?php
	}
?>
</h2>

<?php if(!Empty($msg)){ echo $msg; }
?>

<table>
	<tr>
		<th>Nome</th>
		<th>Login</th>
		<th>Email</th>
		<th>Telefone</th>
		<th>A��es</th>
	</tr>
	<?php 
		$cont=0;
		foreach($query as $row):
	?>
	<tr class="<?php if($cont % 2 == 0) echo 'rowA'; else echo 'rowB'; ?>">
		<td><?php echo $row->nome; ?></td>
		<td><?php echo $row->login; ?></td>
		<td><?php echo $row->email; ?></td>
		<td><?php echo $row->telefone_contato; ?></td>
		<td>
		<?php 			
			$acesso = $this->auth->checkAcessoOpcao('edit');			
			if($acesso)
			{
		?>
			<a href="<?php echo base_url(); ?>index.php/usuario/edit/<?php echo $row->cod_usuario; ?>"><img src="<?php echo base_url();?>public/images/edit.png" alt="Editar Usu�rio" title="Editar Usu�rio" /></a>
		<?php
			}else if($this->session->userdata('codigo_user') == $row->cod_usuario)
			{
		?>
			<a href="<?php echo base_url(); ?>index.php/usuario/edit/<?php echo $row->cod_usuario; ?>"><img src="<?php echo base_url();?>public/images/edit.png" alt="Editar Usu�rio" title="Editar Usu�rio" /></a>	
		<?php
			}
			$acesso1 = $this->auth->checkAcessoOpcao('remove');
			if($acesso1 && ($this->session->userdata('codigo_user') != $row->cod_usuario))
			{
		?>	
			<a onclick="return (confirm('Voc� confirma a Exclus�o?'))" href="<?php echo base_url(); ?>index.php/usuario/remove/<?php echo $row->cod_usuario; ?>"><img src="<?php echo base_url();?>public/images/delete.png" alt="Apagar Usu�rio" title="Apagar Usu�rio" /></a></td>
		<?php }
		?>
	
	</tr>
	<?php
			$cont++;			
		endforeach;
		?>	
</table>
<?php
echo "Total de usu�rios encontrados: <strong>". $num_linhas. "</strong>";
echo "<div id='link' align='center'>".$paginacao."</div>";
?>