<?php
class Forma_farmaceutica_model extends Model
{
	function Forma_farmaceutica_model()
	{
		parent::Model();
	}	
	
	function paginacao($inicio, $pagina)
	{
		$sql = "SELECT * FROM forma_farmaceutica ORDER BY forma_farmaceutica ASC limit ".$inicio. ", ".$pagina;			
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function adicionar($nome)
	{			
		$sql = "INSERT INTO forma_farmaceutica (cod_forma_farmaceutica, forma_farmaceutica)
				VALUES(null, ".$this->db->escape($nome).")";
		$this->db->query($sql);		
	}
	
	function busca_forma_farmaceutica_codigo($id)
	{
		$sql = "SELECT * FROM forma_farmaceutica WHERE cod_forma_farmaceutica = ".$this->db->escape($id);
		$query = $this->db->query($sql);
		return $query->row();
	}
	
	function editar($id, $forma_farmaceutica)	
	{
		$sql = "UPDATE forma_farmaceutica SET forma_farmaceutica = ".$this->db->escape($forma_farmaceutica)."								
				WHERE cod_forma_farmaceutica = ".$this->db->escape($id)."";
		$this->db->query($sql);	
	}
	
	function num_linhas()
	{
		$sql = "SELECT * FROM forma_farmaceutica";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
	function remover($id)
	{
		$sql = "DELETE FROM forma_farmaceutica WHERE cod_forma_farmaceutica = ".$id;
		$this->db->query($sql);		
	}
	
	function num_linhas_busca($busca)
	{
		if(Empty($busca))
		{
			$sql = "SELECT *
				 FROM forma_farmaceutica				
				ORDER BY forma_farmaceutica ASC";	
		}else
		{
			$sql = "SELECT *
				 FROM forma_farmaceutica
				WHERE forma_farmaceutica LIKE '%".$busca."%'				
				ORDER BY forma_farmaceutica ASC ";	
		}
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
	function busca($busca, $inicio, $pagina)
	{
		if(Empty($busca))
		{
			$sql = "SELECT cod_forma_farmaceutica, forma_farmaceutica
				 FROM forma_farmaceutica				
				ORDER BY forma_farmaceutica ASC limit ".$inicio. ", ".$pagina;	
		}else
		{		
			$sql = "SELECT cod_forma_farmaceutica, forma_farmaceutica
				 FROM forma_farmaceutica
				WHERE forma_farmaceutica LIKE '%".$busca."%'				
				ORDER BY forma_farmaceutica ASC limit ".$inicio. ", ".$pagina;	
		}
		$query = $this->db->query($sql);		
		return $query->result();
	}
}
?>
