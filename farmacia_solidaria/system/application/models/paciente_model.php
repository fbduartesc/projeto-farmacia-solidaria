<?php
class Paciente_model extends Model
{
	function Paciente_model()
	{
		parent::Model();
	}	
	
	function adicionar($nome, $endereco, $cep, $bairro, $cidade, $email, $data_nasc, $estado, $tel_residencial, $tel_celular, $tel_comercial)
	{			
		$sql = "INSERT INTO pacientes (cod_paciente, usuarios_cod_usuario, nome, endereco, cep, bairro, cidade, email, data_nasc, data_cadastro, estado, ativo, telefone_residencial, telefone_celular, telefone_comercial)
				VALUES(null, ".$this->session->userdata('codigo_user').", ".$this->db->escape($nome). "," .$this->db->escape($endereco). ", ".$this->db->escape($cep).", ".$this->db->escape($bairro).",".$this->db->escape($cidade).", ".$this->db->escape($email).",".$this->db->escape($data_nasc).", NOW(), ".$this->db->escape($estado).", 1, ".$this->db->escape($tel_residencial).", ".$this->db->escape($tel_celular).", ".$this->db->escape($tel_comercial).")";
		$this->db->query($sql);		
	}	
	
	
	function num_linhas()
	{
		$sql = "SELECT * FROM pacientes WHERE ativo = 1";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
	function paginacao($inicio, $pagina)
	{
		$sql = "SELECT * FROM pacientes p WHERE p.ativo = 1 ORDER BY p.nome ASC limit ".$inicio. ", ".$pagina;			
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function busca_paciente_codigo($id)
	{
		$sql = "SELECT * FROM pacientes WHERE cod_paciente = ".$this->db->escape($id);
		$query = $this->db->query($sql);
		return $query->row();
	}	
	
	function remover($id)
	{
		$sql = "UPDATE pacientes SET ativo = 0 WHERE cod_paciente = ".$id;
		$this->db->query($sql);		
	}
	
	function editar($id, $nome, $endereco, $bairro, $cep, $cidade, $email, $estado, $data_nasc, $tel_residencial, $tel_celular, $tel_comercial)	
	{
		$sql = "UPDATE pacientes SET nome = ".$this->db->escape($nome).",
				endereco = ".$this->db->escape($endereco).",
				cep = ".$this->db->escape($cep).",				
				bairro = ".$this->db->escape($bairro).",
				cidade = ".$this->db->escape($cidade).",
				email = ".$this->db->escape($email).",
				data_nasc = ".$this->db->escape($data_nasc).",
				estado = ".$this->db->escape($estado).",
				telefone_residencial = ".$this->db->escape($tel_residencial).",
				telefone_celular = ".$this->db->escape($tel_celular).",
				telefone_comercial = ".$this->db->escape($tel_comercial)."
				WHERE cod_paciente = ".$this->db->escape($id);
		$this->db->query($sql);	
	}
	
	function num_linhas_busca($busca)
	{
		if(Empty($busca))
		{
			$sql = "SELECT *
				 FROM pacientes				
				ORDER BY nome ASC";	
		}else
		{
			$sql = "SELECT *
				 FROM pacientes
				WHERE nome LIKE '%".$busca."%'
				OR endereco LIKE '%".$busca."%'
				ORDER BY nome ASC ";	
		}
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
	function busca($busca, $inicio, $pagina)
	{
		if(Empty($busca))
		{
			$sql = "SELECT cod_paciente, nome, endereco
				 FROM pacientes				
				ORDER BY nome ASC limit ".$inicio. ", ".$pagina;	
		}else
		{		
			$sql = "SELECT cod_paciente, nome, endereco
				 FROM pacientes
				WHERE nome LIKE '%".$busca."%'
				OR endereco LIKE '%".$busca."%'
				ORDER BY nome ASC limit ".$inicio. ", ".$pagina;	
		}
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
}
?>
