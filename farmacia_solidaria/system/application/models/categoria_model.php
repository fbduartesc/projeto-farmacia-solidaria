<?php
class Categoria_model extends Model
{
	function Categoria_model()
	{
		parent::Model();
	}	
	
	function paginacao($inicio, $pagina)
	{
		$sql = "SELECT * FROM categorias ORDER BY categoria ASC limit ".$inicio. ", ".$pagina;			
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function adicionar($nome)
	{			
		$sql = "INSERT INTO categorias (cod_categoria, categoria)
				VALUES(null, ".$this->db->escape($nome).")";
		$this->db->query($sql);		
	}
	
	function busca_categoria_codigo($id)
	{
		$sql = "SELECT * FROM categorias WHERE cod_categoria = ".$this->db->escape($id);
		$query = $this->db->query($sql);
		return $query->row();
	}
	
	function editar($id, $categoria)	
	{
		$sql = "UPDATE categorias SET categoria = ".$this->db->escape($categoria)."								
				WHERE cod_categoria = ".$this->db->escape($id)."";
		$this->db->query($sql);	
	}
	
	function num_linhas()
	{
		$sql = "SELECT * FROM categorias";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
	function remover($id)
	{
		$sql = "DELETE FROM categorias WHERE cod_categoria = ".$id;
		$this->db->query($sql);		
	}
	
	function num_linhas_busca($busca)
	{
		if(Empty($busca))
		{
			$sql = "SELECT *
				 FROM categorias				
				ORDER BY categoria ASC";	
		}else
		{
			$sql = "SELECT *
				 FROM categorias
				WHERE categoria LIKE '%".$busca."%'				
				ORDER BY categoria ASC ";	
		}
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
	function busca($busca, $inicio, $pagina)
	{
		if(Empty($busca))
		{
			$sql = "SELECT cod_categoria, categoria
				 FROM categorias				
				ORDER BY categoria ASC limit ".$inicio. ", ".$pagina;	
		}else
		{		
			$sql = "SELECT cod_categoria, categoria
				 FROM categorias
				WHERE categoria LIKE '%".$busca."%'				
				ORDER BY categoria ASC limit ".$inicio. ", ".$pagina;	
		}
		$query = $this->db->query($sql);		
		return $query->result();
	}
}
?>
