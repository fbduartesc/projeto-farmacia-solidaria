<?php
class Post_model extends Model
{
	function Post_model()
	{
		parent::Model();
	}
	
	function adicionar($categoria, $titulo, $descricao, $formato, $tamanho)
	{			
		$sql = "INSERT INTO post (cod, categoria_cod, usuarios_cod, titulo, descricao, formato, tamanho, data_postagem)
				VALUES(null, ".$this->db->escape($categoria). "," .$this->session->userdata("codigo_user"). ", ".$this->db->escape($titulo).", ".$this->db->escape($descricao).",".$this->db->escape($formato).",".$this->db->escape($tamanho).", NOW())";
		$this->db->query($sql);		
	}
	
	function adicionar_link($id, $link, $texto)
	{			
		$sql = "INSERT INTO links (post_cod, link, texto)
				VALUES(".$id.",".$this->db->escape($link).",". $this->db->escape($texto).")";
		$this->db->query($sql);		
	}
	
	function todos()
	{
		$sql = "SELECT * FROM post ORDER BY cod DESC";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function paginacao($inicio, $pagina)
	{
		$sql = "SELECT *, DATE_FORMAT(data_postagem, 'em %d de %M de %Y')as data_postagem FROM post p, usuarios u WHERE p.usuarios_cod = u.cod_user ORDER BY p.cod DESC limit ".$inicio. ", ".$pagina;			
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function num_linhas()
	{
		$sql = "SELECT * FROM post ORDER BY cod DESC";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
	function update($foto, $cod)
	{
		$sql = "UPDATE post SET foto = ". $this->db->escape($foto). " WHERE cod = ". $cod;
		$this->db->query($sql);
	}
	
	function ver($id)
	{
		$sql = "SELECT *, DATE_FORMAT(data_postagem, 'em %d %M de %Y')as data_postagem FROM post p, links l, usuarios u WHERE p.cod = ".$this->db->escape($id). "AND l.post_cod = p.cod AND p.usuarios_cod = u.cod_user";
		$query = $this->db->query($sql);
		return $query->row();
	}
	
	function ver_link($id)
	{
		$sql = "SELECT * FROM links l WHERE l.post_cod = ".$this->db->escape($id);
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function busca_link($id, $parte)
	{
		$sql = "SELECT * FROM links l WHERE l.post_cod = ".$this->db->escape($id). " AND l.texto = ".$this->db->escape($parte);
		$query = $this->db->query($sql);
		return $query->row();
	}
	
	function clicks($post_cod, $agente, $ip)
	{
		$sql = "INSERT INTO clicks (cod_click, post_cod, agente, ip)
				VALUES(null, ".$this->db->escape($post_cod). "," .$this->db->escape($agente). ", ".$this->db->escape($ip).")";
		$this->db->query($sql);
	}
	
	function qtde_download($id)
	{
		$sql = "SELECT *, COUNT(*) as qtde 
		           FROM clicks c
				   WHERE c.post_cod = ".$this->db->escape($id)." GROUP BY c.post_cod";
		$query = $this->db->query($sql);
		return $query->row();
	}
	
	function rss()
	{
		$sql = "SELECT cod,titulo,descricao,foto, formato, tamanho, data_postagem 
		          FROM post 
				ORDER BY data_postagem DESC LIMIT 20";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function busca($busca, $inicio, $pagina)
	{
		if(Empty($busca))
		{
			$sql = "SELECT cod, titulo, descricao, DATE_FORMAT(data_postagem, 'Postado em %d %M de %Y')as data_postagem
				 FROM post				
				ORDER BY cod ASC limit ".$inicio. ", ".$pagina;	
		}else
		{		
			$sql = "SELECT cod, titulo, descricao, DATE_FORMAT(data_postagem, 'Postado em %d %M de %Y')as data_postagem
				 FROM post
				WHERE titulo LIKE '%".$busca."%'
				OR descricao LIKE '%".$busca."%'
				ORDER BY cod ASC limit ".$inicio. ", ".$pagina;	
		}
		$query = $this->db->query($sql);		
		return $query->result();
	}
	
	function num_linhas_busca($busca)
	{
		if(Empty($busca))
		{
			$sql = "SELECT *
				 FROM post				
				ORDER BY cod ASC";	
		}else
		{
			$sql = "SELECT *
				 FROM post
				WHERE titulo LIKE '%".$busca."%'
				OR descricao LIKE '%".$busca."%'
				ORDER BY cod ASC ";	
		}
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
}
?>
