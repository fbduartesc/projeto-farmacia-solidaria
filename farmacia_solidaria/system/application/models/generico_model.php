<?php
class Generico_model extends Model
{
	function Generico_model()
	{
		parent::Model();
	}	
	
	function paginacao($inicio, $pagina)
	{
		$sql = "SELECT * FROM genericos ORDER BY generico ASC limit ".$inicio. ", ".$pagina;			
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function adicionar($nome)
	{			
		$sql = "INSERT INTO genericos (cod_generico, generico)
				VALUES(null, ".$this->db->escape($nome).")";
		$this->db->query($sql);		
	}
	
	function busca_generico_codigo($id)
	{
		$sql = "SELECT * FROM genericos WHERE cod_generico = ".$this->db->escape($id);
		$query = $this->db->query($sql);
		return $query->row();
	}
	
	function editar($id, $generico)	
	{
		$sql = "UPDATE genericos SET generico = ".$this->db->escape($generico)."								
				WHERE cod_generico = ".$this->db->escape($id)."";
		$this->db->query($sql);	
	}
	
	function num_linhas()
	{
		$sql = "SELECT * FROM genericos";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
	function remover($id)
	{
		$sql = "DELETE FROM genericos WHERE cod_generico = ".$id;
		$this->db->query($sql);		
	}
	
	function num_linhas_busca($busca)
	{
		if(Empty($busca))
		{
			$sql = "SELECT *
				 FROM genericos				
				ORDER BY generico ASC";	
		}else
		{
			$sql = "SELECT *
				 FROM genericos
				WHERE generico LIKE '%".$busca."%'				
				ORDER BY generico ASC ";	
		}
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
	function busca($busca, $inicio, $pagina)
	{
		if(Empty($busca))
		{
			$sql = "SELECT cod_generico, generico
				 FROM genericos				
				ORDER BY generico ASC limit ".$inicio. ", ".$pagina;	
		}else
		{		
			$sql = "SELECT cod_generico, generico
				 FROM genericos
				WHERE generico LIKE '%".$busca."%'				
				ORDER BY generico ASC limit ".$inicio. ", ".$pagina;	
		}
		$query = $this->db->query($sql);		
		return $query->result();
	}
}
?>
