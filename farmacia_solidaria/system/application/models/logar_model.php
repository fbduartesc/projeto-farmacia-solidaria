<?php
class Logar_model extends Model
{
	function Logar_model()
	{
		parent::Model();
	}
	
	function verifica($usuario, $senha)
	{		
		$sql = "SELECT * FROM usuarios
				WHERE login = ".$this->db->escape($usuario)."
				AND senha = ".$this->db->escape($senha)."
				AND ativo = 1
				";
		$query = $this->db->query($sql);
		return $query->row();
	}
	
	function item_acesso($cod_tipo_usuario)
	{		
		$sql = "SELECT * FROM tipo_usuario_item_acesso as tuia, tipo_usuario as tu, itens_acesso as ia
				WHERE tuia.tipo_usuario_cod_tipo_usuario = ".$this->db->escape($cod_tipo_usuario)."
				AND tuia.tipo_usuario_cod_tipo_usuario = tu.cod_tipo_usuario
				AND tuia.itens_acesso_cod_item = ia.cod_item"
				;				
		$query = $this->db->query($sql);
		return $query->result();
	}
		
		
		
		
	function mostrar()
	{	
		$sql = "SELECT * FROM ramal ORDER BY nr_ramal";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function todos()
	{
		$sql = "SELECT * FROM setor s, ramal r , setor_ramal sr
					WHERE sr.cd_setor = s.cd_setor 
						AND sr.cd_ramal = r.cd_ramal
				ORDER BY s.nm_setor";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function adicionar($ramal)
	{	
		$sql = "INSERT INTO ramal (cd_ramal, nr_ramal)
				VALUES(null, ".$this->db->escape($ramal).")";
		$query = $this->db->query($sql);
		
	}
	
	function view($id)
	{
		$sql = "SELECT cd_ramal, nr_ramal FROM ramal WHERE cd_ramal = ".$id."";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function buscaRamal($desc)
	{		
		$sql = "SELECT cd_ramal, nr_ramal FROM ramal 
				WHERE nr_ramal LIKE '".$desc."%'
				ORDER BY nr_ramal
				";
		$query = $this->db->query($sql); 
		return $query->result();		
	}
	
	function apagaRamal($id)
	{
		$sql = "DELETE FROM ramal
				WHERE cd_ramal = ".$this->db->escape($id)."";
		$query = $this->db->query($sql);
		return $query;
	}
	
	function editaRamal($id, $novo_ramal)	
	{
		$sql = "UPDATE ramal SET nr_ramal = ".$this->db->escape($novo_ramal)."
				WHERE cd_ramal = ".$this->db->escape($id)."";
		$query = $this->db->query($sql);
		return $query;
	}	

	
	function verifica_id($id)
	{
		$sql = "SELECT cd_ramal FROM ramal
				WHERE cd_ramal = ".$this->db->escape($id)."";
		$query = $this->db->query($sql);
		return $query->row();	
	}
	
	function verifica_ramal($desc)
	{
		$sql = "SELECT nr_ramal FROM ramal
				WHERE nr_ramal = ".$this->db->escape($desc)."
				";
		$t = $this->db->query($sql);
		return $t->num_rows();
		
	}
}
?>
