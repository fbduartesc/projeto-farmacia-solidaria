<?php
class Usuario_model extends Model
{
	function Usuario_model()
	{
		parent::Model();
	}	
		
	function paginacao($inicio, $pagina)
	{
		$sql = "SELECT * FROM usuarios u WHERE u.ativo = 1 ORDER BY u.nome ASC limit ".$inicio. ", ".$pagina;			
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function checkAcesso($controller_metodo)
	{
		$sql = "SELECT * FROM itens_acesso i, usuario_item_acesso u
				WHERE u.usuarios_cod_usuario = ".$this->session->userdata('codigo_user')."
				AND u.itens_acesso_cod_item = i.cod_item
				AND i.controller_metodo = ".$this->db->escape($controller_metodo)
				;
		$query = $this->db->query($sql);
		return $query->row();
	}
	
	function adicionar($nome, $login, $senha, $telefone, $email)
	{			
		$sql = "INSERT INTO usuarios (cod_usuario, nome, login, senha, telefone_contato, email, ativo, data_cadastro)
				VALUES(null, ".$this->db->escape($nome). "," .$this->db->escape($login). ", ".$this->db->escape($senha).", ".$this->db->escape($telefone).",".$this->db->escape($email).", 1, NOW())";
		$this->db->query($sql);		
	}
	
	function todas_permissoes()
	{	
		$sql = "SELECT * FROM itens_acesso ORDER BY controller_metodo";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function permissoes_user($id)
	{	
		$sql = "SELECT * FROM usuario_item_acesso u
				WHERE u.usuarios_cod_usuario = ".$id;
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function adicionar_permissao($item, $user)
	{			
		$sql = "INSERT INTO usuario_item_acesso (itens_acesso_cod_item, usuarios_cod_usuario)
				VALUES(".$this->db->escape($item).",".$this->db->escape($user).")";
		$this->db->query($sql);		
	}
	
	function busca_usuario_codigo($id)
	{
		$sql = "SELECT * FROM usuarios WHERE cod_usuario = ".$this->db->escape($id);
		$query = $this->db->query($sql);
		return $query->row();
	}
	
	function num_linhas()
	{
		$sql = "SELECT * FROM usuarios WHERE ativo = 1";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
	function remover($id)
	{
		$sql = "UPDATE usuarios SET ativo = 0 WHERE cod_usuario = ".$id;
		$this->db->query($sql);		
	}
	
	function verifica_usuario($nome)
	{
		$sql = "SELECT * FROM usuarios WHERE nome = ".$this->db->escape($nome);
		$query = $this->db->query($sql);
		return $query->row();
	}
	
	function editar($id, $nome, $telefone, $email)	
	{
		$sql = "UPDATE usuarios SET nome = ".$this->db->escape($nome).",
				telefone_contato = ".$this->db->escape($telefone).",
				email = ".$this->db->escape($email)."				
				WHERE cod_usuario = ".$this->db->escape($id)."";
		$this->db->query($sql);	
	}
	
	function editar_permissoes($item, $id)
	{
		//Apaga todos as permissoes para o usuario
		$this->db->trans_start();		
		$sql = "DELETE FROM usuario_item_acesso
				WHERE usuarios_cod_usuario = ".$this->db->escape($id);
		$this->db->query($sql);
		$this->db->trans_complete();
		
		// Insere novamente as novas permissoes
		if(!Empty($item)){
			$this->db->trans_start();		
			foreach($item as $permissao):
				$this->adicionar_permissao($permissao, $id);		
			endforeach;
			$this->db->trans_complete();
		}
	}
	
	function editar_senha($id, $novasenha)
	{
		$sql = "UPDATE usuarios SET senha = ".$this->db->escape($novasenha)."
				WHERE cod_usuario = ".$this->db->escape($id);
		$this->db->query($sql);
	}
}
?>
