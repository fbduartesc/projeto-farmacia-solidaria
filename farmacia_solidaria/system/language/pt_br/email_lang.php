<?php

$lang['email_must_be_array'] = "O m�todo de valida��o de email deve ser passado como um array.";
$lang['email_invalid_address'] = "Endere�o de email inv�lido: %s";
$lang['email_attachment_missing'] = "N�o foi poss�vel localizar o seguinte anexo de email: %s";
$lang['email_attachment_unredable'] = "N�o foi poss�vel abrir este anexo: %s";
$lang['email_no_recipients'] = "Voc� deve incluir destinat�rios: Para, Cc, ou Bcc";
$lang['email_send_failure_phpmail'] = "N�o foi poss�vel enviar email usando PHP mail(). Seu servidor pode n�o estar configurado para enviar email usando este m�todo.";
$lang['email_send_failure_sendmail'] = "N�o foi poss�vel enviar email usando PHP Sendmail. Seu servidor pode n�o estar configurado para enviar email usando este m�todo.";
$lang['email_send_failure_smtp'] = "N�o foi poss�vel enviar email usando PHP SMTP. Seu servidor pode n�o estar configurado para enviar email usando este m�todo.";
$lang['email_sent'] = "Sua mensagem foi enviada com sucesso usando o seguinte protocolo: %s";
$lang['email_no_socket'] = "N�o foi poss�vel abrir uma conex�o com Sendmail. Por favor confira as configura��es.";
$lang['email_no_hostname'] = "Voc� n�o especificou um servidor SMTP.";
$lang['email_smtp_error'] = "O seguinte erro SMTP foi encontrado: %s";
$lang['email_no_smtp_unpw'] = "Erro: Voc� deve fornecer um usu�rio e senha SMTP.";
$lang['email_filed_smtp_login'] = "Falha ao enviar comando AUTH LOGIN. Erro: %s";
$lang['email_smtp_auth_un'] = "Falha ao autenticar usu�rio. Erro: %s";
$lang['email_smtp_auth_pw'] = "Falha ao autenticar senha. Erro: %s";
$lang['email_smtp_data_failure'] = "N�o foi poss�vel enviar dados: %s";

?>
