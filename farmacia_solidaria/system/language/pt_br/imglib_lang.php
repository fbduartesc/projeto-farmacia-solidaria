<?php

$lang['imglib_source_image_required'] = "Voc� deve especificar uma imagem fonte nas suas prefer�ncias.";
$lang['imglib_gd_required'] = "A biblioteca GD � necess�ria para este recurso.";
$lang['imglib_gd_required_for_props'] = "Seu servidor deve suportear a bibliteca de imagem GD para poder determinar as propriedades da imagem.";
$lang['imglib_unsupported_imagecreate'] = "Seu servidor n�o suporta a fun��o GD necess�ria para processar este tipo de imagem.";
$lang['imglib_gif_not_supported'] = "Imagens GIF frequentemente n�o s�o suportadas por restri��o de licenciamento. Voc� pode ter que usar imagens JPG ou PNG.";
$lang['imglib_jpg_not_supported'] = "Imagens JPG n�o s�o suportadas";
$lang['imglib_png_not_supported'] = "Imagens PNG n�o s�o suportadas";
$lang['imglib_jpg_or_png_required'] = "O protocolo de redimensionamento que voc� especificou nas suas prefer�ncias s� funcionam com imagens do tipo JPEG ou PNG.";
$lang['imglib_copy_error'] = "Um erro foi encontrado ao tentar substituir o arquivo. Por favor, tenha certeza que seu diret�rio possui permiss�o de escrita.";
$lang['imglib_rotate_unsupported'] = "Rota��o de imagem parece n�o ser suportado pelo seu servidor.";
$lang['imglib_libpath_invalid'] = "O caminho para sua biblioteca de imagem n�o est� correto. Por favor, defina o caminho correto em suas prefer�ncias de imagem.";
$lang['imglib_image_process_failed'] = "Processamento de imagem falhou. Verifique se seu servidor suporta o protocolo selecionado e que o caminho para a biblioteca de imagem est� correto.";
$lang['imglib_rotation_angle_required'] = "Um �ngulo de rota��o � necess�rio para rotacionar a imagem.";
$lang['imglib_writing_failed_gif'] = "imagem GIF ";
$lang['imglib_invalid_path'] = "O caminho para a imagem n�o est� correto.";
$lang['imglib_copy_failed'] = "A rotina de c�pia de imagem falhou.";
$lang['imglib_missing_font'] = "N�o foi poss�vel encontrar uma fonte para usar.";

?>
