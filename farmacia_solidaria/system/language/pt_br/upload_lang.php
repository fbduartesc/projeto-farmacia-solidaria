<?php

$lang['upload_userfile_not_set'] = "N�o foi poss�vel encontrar uma vari�vel POST chamada 'userfile'";
$lang['upload_file_exceeds_limit'] = "O arquivo enviado excede o tamanho m�ximo permitido em seu arquivo de configura��o do PHP";
$lang['upload_file_partial'] = "O arquivo foi enviado apenas parcialmente";
$lang['upload_no_file_selected'] = "Voc� n�o selecionou um arquivo para enviar";
$lang['upload_invalid_filetype'] = "O tipo de arquivo que voc� est� tentando enviar n�o � permitido";
$lang['upload_invalid_filesize'] = "O arquivo que voc� est� tentando enviar � maior que o tamanho permitido";
$lang['upload_invalid_dimensions'] = "A imagem que voc� est� tentando enviar excede o comprimento ou altura";
$lang['upload_destination_error'] = "Um problema foi encontrado ao tentar mover o arquivo enviado para o destino final.";
$lang['upload_no_filepath'] = "O caminho para envio parece n�o ser v�lido.";
$lang['upload_no_file_types'] = "Voc� n�o especificou nenhum tipo de arquivo permitido.";
$lang['upload_bad_filename'] = "O nome de arquivo que voc� enviou j� existe no servidor.";
$lang['upload_not_writable'] = "O diret�rio destino de envio parece n�o ter permiss�o de escrita.";

?>
