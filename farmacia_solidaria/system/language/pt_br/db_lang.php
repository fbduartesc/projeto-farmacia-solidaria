<?php

$lang['db_invalid_connection_str'] = 'N�o foi poss�vel determinar as configura��es do banco de dados baseado na string de conex�o enviada.';
$lang['db_unable_to_connect'] = 'N�o foi poss�vel conectar ao seu banco de dados usando as configura��es fornecidas.';
$lang['db_unable_to_select'] = 'N�o foi poss�vel selecionar o banco de dados especificado: %s';
$lang['db_unable_to_create'] = 'N�o foi poss�vel criar o banco de dados especificado: %s';
$lang['db_invalid_query'] = 'A instru��o enviada n�o � v�lida.';
$lang['db_must_set_table'] = 'Voc� deve especificar a tabela do banco de dados a ser usada na sua instru��o.';
$lang['db_must_use_set'] = 'Voc� deve usar o m�todo "set" para atualizar um registro.';
$lang['db_must_use_where'] = 'Atualiza��es n�o s�o permitidas se n�o contiverem um cl�usula "where".';
$lang['db_del_must_use_where'] = 'Remo��es n�o s�o permitidas se n�o contiverem um cl�usula "where".';
$lang['db_field_param_missing'] = 'Para obter os campos o nome da tabela deve ser fornecido como par�metro.';
$lang['db_unsupported_function'] = 'Este recurso n�o est� dispon�vel para o banco de dados que voc� est� usando.';

?>
