<?php

$lang["error_auth"] = "Usu�rio ou Senha inv�lido!";
$lang["error_perm"] = "Usu�rio sem permiss�o de acesso!";
$lang["cd_invalido"] = "C�digo Inv�lido";
$lang["not_permission_admin"] = "�rea restrita ao Administrador!";
$lang["not_permission_revisor"] = "�rea restrita ao Revisor!";
$lang["not_pagina_revisao"] = "Nenhuma pagina para revis�o!";
$lang["invalid_session"] = "Sess�o Inv�lida";
$lang["permission_denied"] = "Permiss�o Negada!";


// --- Empty -- //
$lang["empty_pessoa"] = "Nenhuma pessoa encontrada!";
$lang["empty_capa"] = "Nenhuma capa encontrada!";
$lang["empty_pagina"] = "Nenhuma pagina encontrada!";
$lang["empty_menu"] = "Nenhum menu encontrado";
$lang["empty_sub_menu"] = "Nenhum submenu encontrado";
$lang["empty_excluir"] = "Nenhum �tem foi selecionado!";
$lang["empty_tag"] = "Nenhuma Tag encontrada!";
$lang["empty_file"] = "Nenhum arquivo encontrado";
$lang["empty_pagina_principal"] = "N�o foi encontrada a p�gina principal desta capa!";
$lang["empty_setor"] = "Nenhum setor encontrado!";
$lang["empty_tipo_setor"] = "Nenhum tipo de setor encontrado";
$lang["empty_version_pub"] = "N�o possui nenhuma vers�o publicada.";
$lang["empty_tag_pagina"] = "Essa pagina n�o possui nenhuma tag!";
$lang["empty_capa_user"] = "Nenhuma capa para voc� administrar!";
$lang["empty_pasta"] = "Nenhuma pasta encontrada!";
$lang["empty_menu_pagina"] = "O menu n�o possui nenhuma p�gina!";
$lang["empty_link_relacionado"] = "Nenhum link relacionado a essa p�gina.";
$lang["empty_selos"] = "Nenhum selo encontrado!";
$lang["empty_baners_selos"] = "Nenhum baner ou selo encontrado!";
$lang["empty_baner_capas"] = "Baner n�o possui nenhuma capa!";
$lang["empty_listbox"] = "Nenhum Listbox encontrado!";
$lang["empty_listbox_capa"] = "Capa n�o possui nenhum Listbox!";
$lang["empty_professor_capa"] = "Nenhum professor nesse ano/semestre para essa capa!";
$lang["empty_grade"] = "Nenhuma Grade para essa capa!";
$lang["empty_grade_comp"] = "Nenhuma Grade Complementar para essa capa!";
$lang["empty_coordenacao"] = "Nenhum �tem de Coordena��o foi encontrado!";
$lang["empty_infraestrutura"] = "Nehuma Infra-Estrutura foi encontrada!";
$lang["empty_curso"] = "Nenhum Curso encontrado!";
$lang["empty_capa_tipo"] = "Nenhuma Capa Tipo encontrado!";
$lang["empty_setor_telefone"] = "Nenhum ramal para esse setor!";
$lang["empty_arquivo"] = "Nenhum arquivo encontrado!";
$lang["empty_habilitacao"] = "Nenhuma Habilita��o encontrada!";
$lang["empty_laboratorio"] = "Nenhum Laborat�rio encontrado!";
$lang["empty_local"] = "Nenhum Local encontrado!";
$lang["empty_programa_pesquisa"] = "Nenhum Programa Pesquisa encontrado!";
$lang["empty_projeto_pesquisa"] = "Nenhum Projeto Pesquisa encontrado!";
$lang["empty_area_conhecimento"] = "Nenhuma Area de Conhecimento encontrada!";
$lang["empty_projeto_pesquisa"] = "Nenhum Projeto Pesquisa encontrado!";
$lang["empty_projeto_pesquisa_habilitacao"] = "Nenhuma Habilita��o vinculada ao Projeto Pesquisa!";
$lang["empty_tipo_agenda"] = "Nenhum Tipo Agenda encontrado!";
$lang["empty_fields_javascript"] = "Todos os campos com (*) devem ser preenchidos!";
$lang["empty_agenda"] = "Nenhuma Agenda encontrada!";
$lang["empty_capa_agenda"] = "A Agenda n�o possui nenhuma apa!";
$lang["empty_ramal"] = "Nenhum Ramal encontrado!"; 
$lang["empty_mural"] = "Nenhum Mural encontrado!";
$lang["empty_tipo_mural"] = "Nenhum Tipo Mural encontrado!";
$lang["empty_capa_mural"] = "Nenhuma Capa para esse mural!"; 
$lang["empty_tipo_galeria"] = "Nenhum Tipo Galeria encontrado!";
$lang["empty_galeria"] = "Nenhuma Galeria encontrada!";
$lang["empty_fotos_galeria"] = "Nenhuma foto nesta Galeria!";
$lang["empty_foto"] = "Nenhuma Foto encontrada!";
$lang["empty_capa_galeria"] = "Nenhuma Capa relacionada essa Galeria!";
$lang["empty_comentario_galeria"] = "Nenhum coment�rio relacionado as fotos dessa Galeria!";
$lang["empty_comentario_capa"] = "Nenhum coment�rio relacionado a essa Capa!";
$lang["empty_galeria_capa"] = "Nehuma galeria dispn�vel para essa Capa!";
$lang["empty_solicitacao_galeria"] = "Nenhuma solicita��o encaminhada para essa Capa!";
$lang["empty_comentarios_foto"] = "Nenhum coment�rio relacionado a essa Foto!";
$lang["empty_menu_sem_link"] = "Nenhum menu sem link encontrado!";









$lang["add_success"] = "Cadastro efetuado com sucesso!";
$lang["del_success"] = "Excluido(s) com sucesso!";
$lang["update_success"] = "Alterado com sucesso!";


$lang["efetue_busca"]  = "Favor efetuar uma busca!"; 
$lang["confirm_exclusao"] = "Deseja realmente excluir esses registros?";
$lang["confirm_update"] = "Deseja realmente alterar esses registros?";
$lang["msg_nenhum_item"] = "Favor selecionar um �tem!";
$lang["excluir_pagina_com_menu"] = "A(s) pagina(s) a baixo n�o foram excluidas por possuirem v�nculo com um menu.<br/><br/>";
$lang["status_revisor"] = "Essa p�gina n�o pode ser alterada pois esta sendo revisada. Para edit�-la voc� deve entrar em contato com o Revisor dessa Capa!";
$lang["duplicate_folder"] = "J� existe uma pasta esse nome, favor inserir outro nome.";
$lang["not_selected_folder"] = "Nenhuma pasta foi selecionada!";
$lang["duplicate_pagina_principal"] = "J� existe uma p�gina principal para essa capa";
$lang["error_delete_tipo_setor"] = "Os Tipo Setores abaixo n�o foram removidos!";
$lang["separador_invalido"] = "Separador Inv�lido!";
$lang["email_or_assunto_null"] = "E-mail ou Assunto n�o foram configurados!";

?>