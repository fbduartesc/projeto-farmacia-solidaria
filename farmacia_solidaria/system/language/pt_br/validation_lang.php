<?php

$lang['required'] 		= "O campo %s � obrigat�rio.";
$lang['isset']			= "O campo %s deve conter um valor.";
$lang['valid_email']	= "O campo %s deve conter um endere�o de email v�lido.";
$lang['valid_url'] 		= "O campo %s deve conter uma URL v�lida.";
$lang['min_length']		= "O campo %s deve ter pelo menos %s caracteres de comprimento.";
$lang['max_length']		= "O campo %s n�o pode exceder %s caracteres de comprimento.";
$lang['exact_length']	= "O campo %s deve conter exatamente %s caracteres de comprimento.";
$lang['alpha']			= "O campo %s deve conter apenas caracteres alfa-num�ricos.";
$lang['alpha_numeric']	= "O campo %s deve conter apenas caracteres alfa-num�ricos ou n�meros";
$lang['alpha_dash']	    = "O campo %s deve conter apenas caracteres alfa-num�ricos, n�meros, underscores, e h�fen.";
$lang['numeric']		= "O campo %s deve conter apenas n�mero(s).";
$lang['matches']		= "O campo %s deve ser igual ao campo %s.";
$lang['captcha']        = "O c�digo secreto esta inv�lido";

?>
